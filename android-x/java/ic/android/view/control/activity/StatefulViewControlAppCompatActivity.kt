package ic.android.view.control.activity


import android.content.Context
import android.view.ViewGroup

import ic.android.ui.view.scope.AndroidViewScope


abstract class StatefulViewControlAppCompatActivity<State>
	: StatefulViewControlAppCompatActivityWithEnv<State, AndroidViewScope>()
{

	override fun initEnvironment (context: Context, parentView: ViewGroup?) : AndroidViewScope {
		return AndroidViewScope(
			context = context,
			parentView = parentView
		)
	}

}