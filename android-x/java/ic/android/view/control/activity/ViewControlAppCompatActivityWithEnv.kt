package ic.android.view.control.activity


import android.os.Bundle
import ic.android.ui.view.scope.AndroidViewScope


abstract class ViewControlAppCompatActivityWithEnv<Environment: AndroidViewScope>
	: StatefulViewControlAppCompatActivityWithEnv<Unit, Environment>()
{

	override fun initState() = Unit

	override fun implementParseState (bundle: Bundle) = Unit

	override fun implementSerializeState (state: Unit) = Bundle()

}