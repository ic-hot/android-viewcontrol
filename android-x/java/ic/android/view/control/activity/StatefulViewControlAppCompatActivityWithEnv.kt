package ic.android.view.control.activity


import ic.ifaces.pausable.Pausable
import ic.ifaces.stateful.ext.state

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup

import ic.android.ui.activity.decor.DecoratedAppCompatActivity
import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.util.bundle.ext.getAsBundleOrNull
import ic.android.util.bundle.ext.set


abstract class StatefulViewControlAppCompatActivityWithEnv <State, Environment: AndroidViewScope>
	: ic.android.ui.activity.decor.DecoratedAppCompatActivity()
{


	protected abstract fun initViewController()
		: GenerativeStatefulViewControllerWithEnv<State, Environment>
	;

	protected abstract fun initState() : State

	protected abstract fun initEnvironment (context: Context, parentView: ViewGroup?) : Environment


	protected abstract fun implementParseState (bundle: Bundle) : State

	protected abstract fun implementSerializeState (state: State) : Bundle


	private var viewController
		: GenerativeStatefulViewControllerWithEnv<State, Environment>?
		= null
	;


	override fun onCreate (stateBundle: Bundle?) {
		super.onCreate(stateBundle)
		val viewController = initViewController()
		this.viewController = viewController
		val viewControllerStateBundle = (
			stateBundle?.getAsBundleOrNull("viewControllerState")
		)
		val state = (
			if (viewControllerStateBundle == null) {
				initState()
			} else {
				implementParseState(viewControllerStateBundle)
			}
		)
		val environment = initEnvironment(
			context = this,
			parentView = containerView
		)
		val view = viewController.openWithState(state, environment)
		setContentView(view)
	}


	override fun onStart() {
		super.onStart()
		val viewController = viewController
		if (viewController is Pausable) {
			viewController.resume()
		}
	}


	override fun onStop() {
		super.onStop()
		val viewController = viewController
		if (viewController is Pausable) {
			viewController.pause()
		}
	}


	override fun saveStateToBundle (stateBundle: Bundle) {
		val viewController = viewController
		if (viewController != null) {
			stateBundle["viewControllerState"] = implementSerializeState(viewController.state)
		}
	}


	override fun onDestroy() {
		super.onDestroy()
		viewController!!.close()
		viewController = null
	}


}