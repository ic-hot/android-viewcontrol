package ic.android.ui.view.roundcorners


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.asInt32

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import android.util.AttributeSet

import ic.android.ui.view.layout.frame.FrameLayout
import ic.android.ui.view.control.R


open class RoundCornersView : FrameLayout {


	init {
		clipToPadding = true
	}


	private var cornersRadiusPxField : Int32 = 0

	var cornersRadiusPx : Int32
		get() = cornersRadiusPxField
		set(value) {
			cornersRadiusPxField = value
			updateRadiiAndBackground()
		}
	;


	private var topCornersRadiusPxField : Float32 = Float32(0)

	var topCornersRadiusPx : Float32
		get() = topCornersRadiusPxField
		set(value) {
			topCornersRadiusPxField = value
			updateRadiiAndBackground()
		}
	;


	private lateinit var radii : FloatArray

	private fun updateRadiiAndBackground() {
		radii = floatArrayOf(
			cornersRadiusPx + topCornersRadiusPxField,
			cornersRadiusPx + topCornersRadiusPxField,
			cornersRadiusPx + topCornersRadiusPxField,
			cornersRadiusPx + topCornersRadiusPxField,
			cornersRadiusPx.asFloat32,
			cornersRadiusPx.asFloat32,
			cornersRadiusPx.asFloat32,
			cornersRadiusPx.asFloat32
		)
		background = ShapeDrawable(
			RoundRectShape(
				radii,
				null, null
			)
		).apply {
			paint.color = 0xffffffff.asInt32
		}
	}


	override fun dispatchDraw(canvas: Canvas) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			val clipPath = Path()
			clipPath.addRoundRect(RectF(canvas.clipBounds), radii, Path.Direction.CW)
			canvas.clipPath(clipPath)
		}
		super.dispatchDraw(canvas)
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	{

		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.RoundCornersView)
			cornersRadiusPxField = styledAttributes.getDimensionPixelSize(R.styleable.RoundCornersView_cornersRadius, 0)
			topCornersRadiusPxField = styledAttributes.getDimension(R.styleable.RoundCornersView_topCornersRadius, 0F)
			styledAttributes.recycle()
		}

		updateRadiiAndBackground()

	}


}