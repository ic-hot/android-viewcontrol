@file:Suppress("DEPRECATION")


package ic.android.ui.view.rll.ext


import ic.ifaces.getter.Getter

import ic.android.ui.view.rll.RecyclerLinearLayout


inline fun <Item: Any> RecyclerLinearLayout<Item>.open (

	crossinline generateItemController : () -> RecyclerLinearLayout<Item>.ItemController<*>

) {

	open(
		Getter(generateItemController)
	)

}