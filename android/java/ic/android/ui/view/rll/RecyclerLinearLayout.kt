@file:Suppress("FunctionName", "LeakingThis", "DEPRECATION")


package ic.android.ui.view.rll


import ic.base.kfunctions.DoNothing
import ic.base.primitives.int64.Int64
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.getter.Getter
import ic.pattern.carrier.GenerativeCarrier
import ic.util.recycler.Recycler
import ic.struct.list.List

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View

import ic.android.ui.view.layout.linear.LinearLayout
import ic.android.util.handler.postForEachIndexed
import ic.base.primitives.int64.asInt32
import ic.struct.list.ext.foreach.breakableForEachIndexed


@Deprecated("Use ListController")
open class RecyclerLinearLayout<Item: Any> : LinearLayout {


	private val itemViewCarrierEnvironment = ic.android.ui.viewcarrier.ViewCarrier.Environment(context as Activity, this)


	abstract inner class ItemController
		<ItemViewCarrier: GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
	{

		protected abstract fun initItemViewCarrier() : ItemViewCarrier

		val viewCarrier : ItemViewCarrier = initItemViewCarrier()

		val view = viewCarrier.open(itemViewCarrierEnvironment)

		var index : Int = 0
			internal set (value) {
				field = value
				onIndexChanged(value)
			}
		;

		open fun onIndexChanged (index: Int) {}

	}

	inline fun
		<ItemViewCarrier: GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		ItemController
	(
		viewCarrier : ItemViewCarrier,
		crossinline onIndexChanged : RecyclerLinearLayout<Item>.ItemController<ItemViewCarrier>.(index: Int) -> Unit = {}
	) : ItemController<*> {
		return object : ItemController<ItemViewCarrier>() {
			override fun initItemViewCarrier() = viewCarrier
			override fun onIndexChanged (index: Int) = onIndexChanged.invoke(this, index)
		}
	}


	private lateinit var itemControllerGenerator : Getter<ItemController<*>>

	fun open (generateItemController: Getter<ItemController<*>>) {
		this.itemControllerGenerator = generateItemController
	}


	private val recycler = object : Recycler<
		Item, RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
	> () {

		override fun generateValue()
			: RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		{
			@Suppress("UNCHECKED_CAST")
			return itemControllerGenerator.get()
				as RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
			;
		}

		override fun onSeize (
			key : Item,
			value : RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		) {
			value.viewCarrier.resume()
		}

		override fun onRelease (
			value : RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		) {
			value.viewCarrier.pause()
		}

		override fun onClose (
			value : RecyclerLinearLayout<Item>.ItemController<GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		) {
			value.viewCarrier.close()
		}

	}


	// Entries:


	private var itemsField : List<out Item>? = null

	protected val items : List<out Item> get() = itemsField!!

	open fun setItems (items: List<out Item>) {
		postSetItemsJob?.cancel()
		this.itemsField = items
		removeAllViews()
		recycler.recycleAll()
		items.breakableForEachIndexed { index, item ->
			val itemController = recycler[item]
			addView(itemController.view)
			itemController.viewCarrier.initState(item)
			itemController.onIndexChanged(index.asInt32)
		}
	}

	fun updateAllItems() {
		setItems(itemsField ?: return)
	}


	private var postSetItemsJob : Cancelable? = null

	fun postSetItems (
		items : List<Item>,
		delayMs : Int64 = 256,
		onFinish : () -> Unit = DoNothing
	) : Cancelable? {
		postSetItemsJob?.cancel()
		this.itemsField = items
		removeAllViews()
		recycler.recycleAll()
		postSetItemsJob = items.postForEachIndexed (
			delayMs = delayMs,
			onFinish = {
				postSetItemsJob = null
				onFinish()
			}
		) { index, item ->
			val itemController = recycler[item]
			addView(itemController.view)
			itemController.viewCarrier.initState(item)
			itemController.onIndexChanged(index)
		}
		return postSetItemsJob
	}


	open fun close() {
		removeAllViews()
		postSetItemsJob?.cancel()
		postSetItemsJob = null
		this.itemsField = null
		recycler.close()
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}