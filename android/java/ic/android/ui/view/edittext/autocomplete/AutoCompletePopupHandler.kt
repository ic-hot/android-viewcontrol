package ic.android.ui.view.edittext.autocomplete


import android.view.View

import ic.ifaces.cancelable.Cancelable


abstract class AutoCompletePopupHandler {

	abstract fun inflateAutoCompletePopupMenu (

		input : String,

		onNoOptions : () -> Unit,

		onViewCreated : (View) -> Unit,

		onOptionSelected : (String) -> Unit

	) : Cancelable?

}