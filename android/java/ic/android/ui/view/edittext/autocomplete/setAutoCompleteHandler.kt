@file:Suppress("DEPRECATION")


package ic.android.ui.view.edittext.autocomplete


import ic.android.ui.view.edittext.EditText
import ic.android.ui.popup.view.showPopup
import ic.ifaces.cancelable.Cancelable


fun EditText.setAutoCompletePopupHandler (autoCompletePopupHandler: AutoCompletePopupHandler) {

	var inflateAutoCompletePopupMenuJob : Cancelable? = null

	fun startInflateAutoCompletePopupMenu (input: String) {

		lateinit var closePopup : () -> Unit

		inflateAutoCompletePopupMenuJob = autoCompletePopupHandler.inflateAutoCompletePopupMenu(
			input = input,
			onNoOptions = {},
			onViewCreated = { view ->
				showPopup(
					popupCarrier = ic.android.ui.popup.PopupCarrier(
						initViewCarrier = {
							closePopup = ::closePopup
							object : ic.android.ui.viewcarrier.GenerativeViewCarrier.UnitState() {
								override fun initSubject() = view
							}
						}
					),
					toOverlapAnchor = true
				)
			},
			onOptionSelected = { option ->
				value = option
				closePopup()
			}
		)

	}

	setOnValueChangedAction("ic.android.ui.view.edittext.setAutoCompleteHandler") { _, newValue, invokedByUserInput ->

		inflateAutoCompletePopupMenuJob?.cancel()
		inflateAutoCompletePopupMenuJob = null

		if (invokedByUserInput) {
			startInflateAutoCompletePopupMenu(newValue)
		}

	}

}