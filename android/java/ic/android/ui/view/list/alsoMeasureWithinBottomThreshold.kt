@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.base.primitives.float32.Float32

import ic.android.ui.view.list.VerticalRecyclerListView
import ic.base.primitives.int32.Int32


internal fun <Item: Any> VerticalRecyclerListView<Item>.alsoMeasureWithinBottomThreshold (

	bottomVisibilityThresholdPx : Float32,

	itemsCount : Int,

	heightPx : Int,

	firstInvisibleWithinPaddingItemIndex : Int,
	firstInvisibleWithinPaddingItemPositionPx : Int32

) {

	var itemIndex : Int = firstInvisibleWithinPaddingItemIndex
	var itemPositionPx : Int32 = firstInvisibleWithinPaddingItemPositionPx

	while (itemIndex < itemsCount && itemPositionPx < heightPx + bottomVisibilityThresholdPx) {
		val childHeight = measureChild(itemIndex)
		itemIndex++
		itemPositionPx += childHeight
	}

	firstInvisibleWithinThresholdItemIndex = itemIndex

}