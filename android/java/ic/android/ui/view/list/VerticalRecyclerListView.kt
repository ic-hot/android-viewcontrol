@file:Suppress("LeakingThis", "PARAMETER_NAME_CHANGED_ON_OVERRIDE", "FunctionName", "DEPRECATION")


package ic.android.ui.view.list


import ic.math.*
import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.ext.findIndexOrNull
import ic.util.recycler.Recycler
import ic.util.time.Time
import ic.util.time.funs.now
import ic.struct.list.List

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

import ic.gui.anim.interpolator.SmoothInterpolator
import ic.android.ui.touch.TouchEvent
import ic.android.ui.view.group.ViewGroup
import ic.android.ui.touch.drag.VerticalDragHandler
import ic.android.ui.touch.view.dispatchViewTouchEvent
import ic.android.ui.view.ext.activity
import ic.android.ui.view.ext.translationZPx
import ic.android.ui.view.list.deprecated.layoutChildren
import ic.android.ui.view.list.deprecated.measureCorrectScrollStateAndRecycle
import ic.android.ui.viewcarrier.ViewCarrier
import ic.android.util.units.dpToPx
import ic.android.util.units.pxToDp
import ic.gui.anim.dynamic.DynamicValueAnimatorForScrollDp
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int64.asInt32
import ic.ifaces.getter.Getter
import ic.struct.list.ext.foreach.breakableForEach
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.funs.abs
import ic.math.ext.clamp
import ic.struct.list.ext.atLeastOneInRange
import ic.struct.list.ext.get
import ic.struct.list.ext.getOrNull


@Deprecated("Use VerticalListView")
open class VerticalRecyclerListView <Item: Any> : ViewGroup {


	// Abstract:

	protected open val topVisibilityThresholdPx 	: Float32 get() = Float32(0)
	protected open val bottomVisibilityThresholdPx 	: Float32 get() = Float32(0)

	protected open val minimumExpectedItemHeightPx : Int32 get() = dpToPx(64)


	init {
		clipToPadding = false
	}


	private lateinit var itemControllerGenerator : Getter<ItemController<*>>

	fun open (itemControllerGenerator: Getter<ItemController<*>>) {
		this.itemControllerGenerator = itemControllerGenerator
	}


	// Recycler:

	private val itemViewCarrierEnvironment = ic.android.ui.viewcarrier.ViewCarrier.Environment(activity, this)

	abstract inner class ItemController<
		ItemViewCarrier: GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>
	> {

		protected abstract fun initViewCarrier() : ItemViewCarrier

		val viewCarrier : ItemViewCarrier = initViewCarrier()

		val view = viewCarrier.open(itemViewCarrierEnvironment)

		var index : Int = 0
			internal set (value) {
				field = value
				onIndexChanged(value)
			}
		;

		open fun onIndexChanged (index: Int) {}

		open fun setTranslationZPx (translationZPx: Float32) {
			view.translationZPx = translationZPx
		}

	}

	inline fun
		<ItemViewCarrier: GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>>
		ItemController
	(
		viewCarrier : ItemViewCarrier,
		crossinline onIndexChanged : ItemController<ItemViewCarrier>.(index: Int) -> Unit = {},
		crossinline setTranslationZPx : ItemController<ItemViewCarrier>.(translationZPx: Float32) -> Unit = {
			view.translationZPx = translationZPx
		}
	) : ItemController<ItemViewCarrier> {
		return object : ItemController<ItemViewCarrier>() {
			override fun initViewCarrier() = viewCarrier
			override fun onIndexChanged (index: Int) = onIndexChanged.invoke(this, index)
			override fun setTranslationZPx (translationZPx: Float32) = setTranslationZPx.invoke(this, translationZPx)
		}
	}


	internal val recycler = object : Recycler <
		Item, ItemController<GenerativeCarrier<View, Item, ViewCarrier.Environment, *>>
	> () {

		override fun generateValue()
			: VerticalRecyclerListView<Item>.ItemController<
			GenerativeCarrier<View, Item, ViewCarrier.Environment, *>
			>
		{
			@Suppress("UNCHECKED_CAST")
			return itemControllerGenerator.get()
				as VerticalRecyclerListView<Item>.ItemController<
				GenerativeCarrier<View, Item, ViewCarrier.Environment, *>
				>
			;
		}

		override fun onSeize (
			key : Item,
			value : VerticalRecyclerListView<Item>.ItemController<
				GenerativeCarrier<View, Item, ViewCarrier.Environment, *>
				>
		) {
			value.viewCarrier.resume()
		}

		override fun onRelease (
			value : VerticalRecyclerListView<Item>.ItemController<
				GenerativeCarrier<View, Item, ViewCarrier.Environment, *>
				>
		) {
			value.viewCarrier.pause()
		}

		override fun onClose (
			value : VerticalRecyclerListView<Item>.ItemController<
				GenerativeCarrier<View, Item, ViewCarrier.Environment, *>
				>
		) {
			value.viewCarrier.close()
		}

	}


	// Basic recycler list view state:

	internal var itemsField : List<Item> = List()

	internal var firstVisibleWithinPaddingItemIndexField : Int = 0

	internal var firstVisibleWithinPaddingItemPositionRelativeToPaddingPx : Int32 = 0

	internal var continuousScrollPositionPxField : Int32 = 0

	internal var pullToRefreshPositionPxField : Float32 = Float32(0)


	// Intermediate state after measuring:

	internal var firstVisibleWithinThresholdItemIndex 	: Int = 0
	internal var firstInvisibleWithinThresholdItemIndex : Int = 0


	// Layout:

	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val widthPx 	= MeasureSpec.getSize(widthMeasureSpec)
		val heightPx 	= MeasureSpec.getSize(heightMeasureSpec)
		setMeasuredDimension(widthPx, heightPx)
		removeAllViewsInLayout()
		measureCorrectScrollStateAndRecycle(
			topVisibilityThresholdPx = topVisibilityThresholdPx,
			bottomVisibilityThresholdPx = bottomVisibilityThresholdPx
		)
		for (i in firstVisibleWithinThresholdItemIndex until firstInvisibleWithinThresholdItemIndex) {
			val itemController = recycler[itemsField[i]]
			addViewInLayout(itemController.view, childCount, itemController.view.layoutParams)
		}
	}

	override fun onLayout (changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		layoutChildren(
			widthPx = right - left, heightPx = bottom - top
		)
	}


	// Scroll animation:

	private val continuousScrollPositionDpAnimator = DynamicValueAnimatorForScrollDp { position, _ ->
		val biasedScrollPositionPx = dpToPx(position)
		val oldScrollPositionPx = continuousScrollPositionPxField
		val scrollShiftPx = biasedScrollPositionPx - oldScrollPositionPx
		firstVisibleWithinPaddingItemPositionRelativeToPaddingPx -= scrollShiftPx
		continuousScrollPositionPxField = biasedScrollPositionPx
		measureCorrectScrollStateAndRecycle(
			topVisibilityThresholdPx = topVisibilityThresholdPx,
			bottomVisibilityThresholdPx = bottomVisibilityThresholdPx
		)
		requestLayout()
		val correctedScrollPositionPx = continuousScrollPositionPxField
		val correctedScrollShiftPx = correctedScrollPositionPx - oldScrollPositionPx
		onScrollAction(correctedScrollShiftPx)
		if (abs(correctedScrollPositionPx - biasedScrollPositionPx) >= Float32(1) / 16) {
			correct(
				position = pxToDp(correctedScrollPositionPx),
				velocityInvS = Float32(0)
			)
		}
	}

	fun scrollBy (scrollShiftPx : Float32) {
		continuousScrollPositionDpAnimator.setStationary(
			continuousScrollPositionDpAnimator.getPosition() + pxToDp(scrollShiftPx)
		)
	}


	// Touch:

	private sealed class TouchState {
		object Initial 			: TouchState()
		object Scroll 			: TouchState()
		object SwipeToRefresh 	: TouchState()
	}

	private var continuousScrollPositionAtDownDp : Float32 = Float32(0)

	private var downTime : Time = Time.EpochStart

	private var touchState : TouchState = TouchState.Initial

	private fun draggableToVisibleSwipeToRefreshPositionPx (draggableSwipeToRefreshPositionPx: Float32) : Float32 {
		return if (draggableSwipeToRefreshPositionPx < pullToRefreshFullPositionPx) {
			draggableSwipeToRefreshPositionPx.clamp(0, pullToRefreshFullPositionPx)
		} else {
			draggableSwipeToRefreshPositionPx
				.normalize(pullToRefreshFullPositionPx, pullToRefreshLimitPositionPx)
				.compactify()
				.denormalize(pullToRefreshFullPositionPx, pullToRefreshLimitPositionPx)
			;
		}
	}

	private fun animateSwipeToRefreshPositionBack() {
		ic.gui.anim.animateValue(
			from = pullToRefreshPositionPxField,
			to = Float32(0),
			interpolator = SmoothInterpolator,
			onStop = {},
			onBreak = {},
			onCancel = {},
			onComplete = {},
			onFrame = { swipeToRefreshPositionPx ->
				this.pullToRefreshPositionPxField = swipeToRefreshPositionPx
				requestLayout()
				onSwipeToRefreshPositionChangedAction(swipeToRefreshPositionPx)
			}
		)
	}

	override val intrinsicTouchHandler = object : VerticalDragHandler() {
		override fun onDragEvent (dragEvent: TouchEvent) {
			when (dragEvent) {
				is TouchEvent.Down -> {
					continuousScrollPositionAtDownDp = pxToDp(continuousScrollPositionPxField)
					downTime = now
					continuousScrollPositionDpAnimator.stopNonBlockingIfNeeded()
					touchState = TouchState.Initial
				}
				is TouchEvent.Move -> {
					if (touchState == TouchState.Initial) {
						if (
							isPullToRefreshEnabled &&
							firstItemPositionRelativeToPaddingPx == 0 &&
							dragEvent.relativePositionPx.y > 0
						) {
							touchState = TouchState.SwipeToRefresh
						} else {
							touchState = TouchState.Scroll
							continuousScrollPositionDpAnimator.seize(
								downPosition = continuousScrollPositionAtDownDp,
								downTimeEpochMs = downTime.epochMs
							)
						}
					}
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							continuousScrollPositionDpAnimator.move(
								continuousScrollPositionAtDownDp - dragEvent.relativePositionDp.y
							)
						}
						TouchState.SwipeToRefresh -> {
							pullToRefreshPositionPxField = draggableToVisibleSwipeToRefreshPositionPx(
								dragEvent.relativePositionPx.y.asFloat32
							)
							requestLayout()
							onSwipeToRefreshPositionChangedAction(pullToRefreshPositionPxField)
						}
					}
				}
				is TouchEvent.Up -> {
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							continuousScrollPositionDpAnimator.release()
							onScrollReleaseAction()
						}
						TouchState.SwipeToRefresh -> {
							continuousScrollPositionDpAnimator.stopNonBlockingIfNeeded()
							animateSwipeToRefreshPositionBack()
							onSwipeToRefreshAction()
						}
					}
				}
				is TouchEvent.Cancel -> {
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							continuousScrollPositionDpAnimator.release()
							continuousScrollPositionDpAnimator.stopNonBlockingIfNeeded()
						}
						TouchState.SwipeToRefresh -> {
							animateSwipeToRefreshPositionBack()
						}
					}
				}
			}
		}
		override val isDragAnimationRunning get() = !continuousScrollPositionDpAnimator.isStationary
	}

	override fun dispatchTouchEvent (motionEvent: MotionEvent) : Boolean {
		return dispatchViewTouchEvent(
			view = this,
			touchHandler = touchHandler,
			motionEvent = motionEvent,
			superDispatchTouchEvent = { super.dispatchTouchEvent(motionEvent) }
		)
	}


	// List actions:

	protected val items : List<Item> get() = itemsField

	fun setItems (items: List<Item>) {
		val firstVisibleItem = itemsField.getOrNull(firstVisibleWithinPaddingItemIndexField)
		itemsField = items
		firstVisibleWithinPaddingItemIndexField = items.findIndexOrNull { it == firstVisibleItem }?.asInt32 ?: 0
		requestLayout()
	}

	fun close() {
		setItems(List())
	}


	// First visible item index:

	val firstVisibleItemIndex : Int get() = firstVisibleWithinPaddingItemIndexField


	// Continuous scroll position:

	val continuousScrollPositionPx get() = continuousScrollPositionPxField

	inline val continuousScrollPositionDp get() = pxToDp(continuousScrollPositionPx)


	// Continuous scroll target position:

	var continuousScrollTargetPositionPx
		get() = dpToPx(continuousScrollPositionDpAnimator.getEndPosition())
		set(value) {
			continuousScrollPositionDpAnimator.setEndPosition(pxToDp(value))
		}
	;

	var continuousScrollTargetPositionDp
		get() = pxToDp(continuousScrollTargetPositionPx)
		set(value) { continuousScrollTargetPositionPx = dpToPx(value) }
	;


	// First item position:

	val firstItemPositionRelativeToPaddingPx : Int32
		get() {
		return if (firstVisibleWithinPaddingItemIndexField == 0) {
			firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
		} else {
			-minimumExpectedItemHeightPx
		}
	}

	val firstItemPositionRelativeToPaddingDp get() = pxToDp(firstItemPositionRelativeToPaddingPx)


	// Swipe to refresh:

	var isPullToRefreshEnabled : Boolean = false

	val pullToRefreshPositionPx get() = pullToRefreshPositionPxField

	protected open val pullToRefreshFullPositionPx 	: Float32 get() = Float32(0)
	protected open val pullToRefreshLimitPositionPx	: Float32 get() = Float32(0)


	// Scroll listeners:

	private var onScrollAction : (scrollShiftPx: Int32) -> Unit = {}

	fun setOnScrollAction (
		toCallAtOnce : Boolean = false,
		onScrollAction : (scrollShiftPx: Int32) -> Unit
	) {
		this.onScrollAction = onScrollAction
		if (toCallAtOnce) {
			onScrollAction(0)
		}
	}

	private var onScrollReleaseAction : () -> Unit = {}
	fun setOnScrollReleaseAction (onScrollReleaseAction: () -> Unit) { this.onScrollReleaseAction = onScrollReleaseAction }


	// Swipe to refresh listeners:

	private var onSwipeToRefreshPositionChangedAction : (swipeToRefreshPositionPx: Float32) -> Unit = {}
	fun setOnSwipeToRefreshPositionChangedAction (
		onSwipeToRefreshPositionChangedAction : (swipeToRefreshPositionPx: Float32) -> Unit
	) {
		this.onSwipeToRefreshPositionChangedAction = onSwipeToRefreshPositionChangedAction
	}

	private var onSwipeToRefreshAction : () -> Unit = {}
	fun setOnPullToRefreshAction (onSwipeToRefreshAction : () -> Unit) {
		this.onSwipeToRefreshAction = onSwipeToRefreshAction
	}


	fun updateAllItems() {
		itemsField.breakableForEach { item ->
			if (
				itemsField.atLeastOneInRange(
					firstVisibleWithinThresholdItemIndex, firstInvisibleWithinThresholdItemIndex
				) { it == item }
			) {
				recycler[item].viewCarrier.initState(item)
			}
		}
	}


	// Necessary constructor:

	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}