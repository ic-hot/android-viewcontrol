@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.base.primitives.int32.Int32

import android.view.View
import ic.android.ui.view.list.VerticalRecyclerListView
import ic.struct.list.ext.get


private fun <Item: Any> VerticalRecyclerListView<Item>.measureChild (child: View) : Int {

	val widthPx = measuredWidth

	child.measure(
		View.MeasureSpec.makeMeasureSpec(widthPx, View.MeasureSpec.EXACTLY),
		View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
	)

	return child.measuredHeight

}


internal fun <Item: Any> VerticalRecyclerListView<Item>.measureChild (
	itemIndex: Int32
) : Int32 {
	val item = itemsField[itemIndex]
	val itemController = recycler[item]
	itemController.index = itemIndex
	itemController.viewCarrier.initStateIfNeeded(item)
	itemController.viewCarrier.resumeIfNeeded()
	return measureChild(itemController.view)
}