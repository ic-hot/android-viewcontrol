@file:Suppress("LeakingThis")


package ic.android.ui.view.list


import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.design.control.list.ListController
import ic.ifaces.getter.Getter
import ic.ifaces.pausable.Pausable
import ic.ifaces.stateful.ext.state
import ic.math.*
import ic.struct.list.ext.findIndexOrNull
import ic.struct.list.List
import ic.util.recycler.Recycler
import ic.util.time.Time
import ic.util.time.funs.now
import ic.gui.anim.animateValue
import ic.gui.anim.dynamic.DynamicValueAnimatorForScrollDp
import ic.gui.anim.interpolator.SmoothInterpolator
import ic.android.ui.touch.TouchEvent
import ic.android.ui.view.group.ViewGroup
import ic.android.ui.touch.drag.VerticalDragHandler
import ic.android.ui.touch.view.dispatchViewTouchEvent
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.ui.view.list.impl.getEntry
import ic.android.ui.view.list.impl.layoutChildren
import ic.android.ui.view.list.impl.measureCorrectScrollStateAndRecycle
import ic.android.util.units.dpToPx
import ic.android.util.units.pxToDp
import ic.base.primitives.int64.asInt32
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.funs.abs
import ic.math.ext.clamp
import ic.struct.list.ext.getOrNull


@Deprecated("Use ic.gui or normal RecyclerView")
open class VerticalListView : ViewGroup, ListController<Any?> {


	var topVisibilityThresholdPx 		: Float32 = Float32(0)
	var bottomVisibilityThresholdPx 	: Float32 = Float32(0)

	var minimumExpectedItemHeightPx 	: Int32 = dpToPx(64)


	init {
		clipToPadding = false
	}


	private lateinit var itemControllerGenerator : Getter<ItemController<Any?>>


	fun <Item> open (
		itemControllerGenerator: Getter<ItemController<Item>>
	) : ListController<Item> {
		@Suppress("UNCHECKED_CAST")
		this.itemControllerGenerator = itemControllerGenerator as Getter<ItemController<Any?>>
		@Suppress("UNCHECKED_CAST")
		return this as ListController<Item>
	}


	// Recycler:

	private val viewEnvironment = AndroidViewScope(
		context = context,
		parentView = this
	)

	internal inner class Entry {
		val itemController : ItemController<*> = itemControllerGenerator.get()
		val view = itemController.open(environment = viewEnvironment)
	}


	internal val recycler = Recycler<Any?, Entry> (

		generateValue = { Entry() },

		onSeize = { item, entry ->
			val itemViewController = entry.itemController
			itemViewController.state = item
			if (itemViewController is Pausable) {
				itemViewController.resume()
			}
		},

		onRelease = { entry ->
			val itemViewController = entry.itemController
			if (itemViewController is Pausable) {
				itemViewController.pause()
			}
		},

		onClose = { entry ->
			entry.itemController.close()
		}

	)


	// Basic recycler list view state:

	internal var itemsField : List<out Any?> = List()

	internal var firstVisibleWithinPaddingItemIndexField : Int32 = 0

	internal var firstVisibleWithinPaddingItemPositionRelativeToPaddingPx : Int32 = 0

	internal var continuousScrollPositionPxField : Int32 = 0

	internal var pullToRefreshPositionPxField : Float32 = Float32(0)


	// Intermediate state after measuring:

	internal var firstVisibleWithinThresholdItemIndex 	: Int = 0
	internal var firstInvisibleWithinThresholdItemIndex : Int = 0


	// Layout:

	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val widthPx 	= MeasureSpec.getSize(widthMeasureSpec)
		val heightPx 	= MeasureSpec.getSize(heightMeasureSpec)
		setMeasuredDimension(widthPx, heightPx)
		removeAllViewsInLayout()
		measureCorrectScrollStateAndRecycle(
			topVisibilityThresholdPx = topVisibilityThresholdPx,
			bottomVisibilityThresholdPx = bottomVisibilityThresholdPx
		)
		for (i in firstVisibleWithinThresholdItemIndex until firstInvisibleWithinThresholdItemIndex) {
			val entry = getEntry(itemIndex = i, toApplyState = false)
			addViewInLayout(entry.view, childCount, entry.view.layoutParams)
		}
	}

	override fun onLayout (changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		layoutChildren(
			widthPx = right - left, heightPx = bottom - top
		)
	}


	// Scroll animation:

	private val scrollDpAnimator = DynamicValueAnimatorForScrollDp { position, _ ->
		val biasedScrollPositionPx = dpToPx(position)
		val oldScrollPositionPx = continuousScrollPositionPxField
		val scrollShiftPx = biasedScrollPositionPx - oldScrollPositionPx
		firstVisibleWithinPaddingItemPositionRelativeToPaddingPx -= scrollShiftPx
		continuousScrollPositionPxField = biasedScrollPositionPx
		measureCorrectScrollStateAndRecycle(
			topVisibilityThresholdPx = topVisibilityThresholdPx,
			bottomVisibilityThresholdPx = bottomVisibilityThresholdPx
		)
		requestLayout()
		val correctedScrollPositionPx = continuousScrollPositionPxField
		val correctedScrollShiftPx = correctedScrollPositionPx - oldScrollPositionPx
		onScrollAction(correctedScrollShiftPx)
		if (abs(correctedScrollPositionPx - biasedScrollPositionPx) >= Float32(1) / 16) {
			this.correct(
				position = pxToDp(correctedScrollPositionPx),
				velocityInvS = Float32(0)
			)
		}
	}

	fun scrollBy (scrollShiftPx : Float32) {
		scrollDpAnimator.setStationary(
			position = scrollDpAnimator.getPosition() + pxToDp(scrollShiftPx)
		)
	}


	// Touch:

	private sealed class TouchState {
		object Initial 			: TouchState()
		object Scroll 			: TouchState()
		object SwipeToRefresh 	: TouchState()
	}

	private var continuousScrollPositionAtDownDp : Float32 = Float32(0)

	private var downTime : Time = Time.EpochStart

	private var touchState : TouchState = TouchState.Initial

	private fun draggableToVisibleSwipeToRefreshPositionPx (draggableSwipeToRefreshPositionPx: Float32) : Float32 {
		return if (draggableSwipeToRefreshPositionPx < pullToRefreshFullPositionPx) {
			draggableSwipeToRefreshPositionPx.clamp(0, pullToRefreshFullPositionPx)
		} else {
			draggableSwipeToRefreshPositionPx
				.normalize(pullToRefreshFullPositionPx, pullToRefreshLimitPositionPx)
				.compactify()
				.denormalize(pullToRefreshFullPositionPx, pullToRefreshLimitPositionPx)
			;
		}
	}

	private fun animateSwipeToRefreshPositionBack() {
		animateValue(
			from = pullToRefreshPositionPxField,
			to = Float32(0),
			interpolator = SmoothInterpolator,
			onStop = {},
			onBreak = {},
			onCancel = {},
			onComplete = {},
			onFrame = { swipeToRefreshPositionPx ->
				this.pullToRefreshPositionPxField = swipeToRefreshPositionPx
				requestLayout()
				onSwipeToRefreshPositionChangedAction(swipeToRefreshPositionPx)
			}
		)
	}

	override val intrinsicTouchHandler = object : VerticalDragHandler() {
		override fun onDragEvent (dragEvent: TouchEvent) {
			when (dragEvent) {
				is TouchEvent.Down -> {
					continuousScrollPositionAtDownDp = pxToDp(continuousScrollPositionPxField)
					downTime = now
					scrollDpAnimator.stopNonBlockingIfNeeded()
					touchState = TouchState.Initial
				}
				is TouchEvent.Move -> {
					if (touchState == TouchState.Initial) {
						if (
							isPullToRefreshEnabled &&
							firstItemPositionRelativeToPaddingPx == 0 &&
							dragEvent.relativePositionPx.y > 0
						) {
							touchState = TouchState.SwipeToRefresh
						} else {
							touchState = TouchState.Scroll
							scrollDpAnimator.seize(
								downPosition = continuousScrollPositionAtDownDp,
								downTimeEpochMs = downTime.epochMs
							)
						}
					}
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							scrollDpAnimator.move(
								continuousScrollPositionAtDownDp - dragEvent.relativePositionDp.y
							)
						}
						TouchState.SwipeToRefresh -> {
							pullToRefreshPositionPxField = draggableToVisibleSwipeToRefreshPositionPx(
								dragEvent.relativePositionPx.y.asFloat32
							)
							requestLayout()
							onSwipeToRefreshPositionChangedAction(pullToRefreshPositionPxField)
						}
					}
				}
				is TouchEvent.Up -> {
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							scrollDpAnimator.release()
							onScrollReleaseAction()
						}
						TouchState.SwipeToRefresh -> {
							scrollDpAnimator.stopNonBlockingIfNeeded()
							animateSwipeToRefreshPositionBack()
							onSwipeToRefreshAction()
						}
					}
				}
				is TouchEvent.Cancel -> {
					when (touchState) {
						TouchState.Initial -> {}
						TouchState.Scroll -> {
							scrollDpAnimator.stopNonBlockingIfNeeded()
						}
						TouchState.SwipeToRefresh -> {
							animateSwipeToRefreshPositionBack()
						}
					}
				}
			}
		}
		override val isDragAnimationRunning get() = !scrollDpAnimator.isStationary
	}

	override fun dispatchTouchEvent (motionEvent: MotionEvent) : Boolean {
		return dispatchViewTouchEvent(
			view = this,
			touchHandler = touchHandler,
			motionEvent = motionEvent,
			superDispatchTouchEvent = { super.dispatchTouchEvent(motionEvent) }
		)
	}


	// List actions:

	override fun getState() = itemsField

	override fun setState (state: List<out Any?>) {
		val firstVisibleItem = itemsField.getOrNull(firstVisibleWithinPaddingItemIndexField)
		itemsField = state
		state.findIndexOrNull { it == firstVisibleItem }?.let {
			firstVisibleWithinPaddingItemIndexField = it.asInt32
		}
		if (firstVisibleWithinPaddingItemIndexField >= state.length) {
			firstVisibleWithinPaddingItemIndexField = state.length.asInt32 - 1
		}
		if (state.length == 0L) {
			firstVisibleWithinPaddingItemIndexField = 0
		}
		requestLayout()
	}

	override fun resume() {
		// TODO
	}

	override fun pause() {
		// TODO
	}


	override fun close() {
		state = List()
		recycler.close()
		scrollDpAnimator.setStationary(Float32(0))
	}


	// First visible item index:

	val firstVisibleItemIndex : Int get() = firstVisibleWithinPaddingItemIndexField


	// Continuous scroll position:

	val continuousScrollPositionPx get() = continuousScrollPositionPxField

	inline val continuousScrollPositionDp get() = pxToDp(continuousScrollPositionPx)


	// Continuous scroll target position:

	var continuousScrollTargetPositionPx
		get() = dpToPx(scrollDpAnimator.getEndPosition())
		set(value) {
			scrollDpAnimator.setEndPosition(pxToDp(value))
		}
	;

	var continuousScrollTargetPositionDp
		get() = pxToDp(continuousScrollTargetPositionPx)
		set(value) { continuousScrollTargetPositionPx = dpToPx(value) }
	;


	// First item position:

	val firstItemPositionRelativeToPaddingPx : Int32
		get() {
		return if (firstVisibleWithinPaddingItemIndexField == 0) {
			firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
		} else {
			-minimumExpectedItemHeightPx
		}
	}

	val firstItemPositionRelativeToPaddingDp get() = pxToDp(firstItemPositionRelativeToPaddingPx)


	// Swipe to refresh:

	var isPullToRefreshEnabled : Boolean = false

	val pullToRefreshPositionPx get() = pullToRefreshPositionPxField

	var pullToRefreshFullPositionPx 	: Float32 = Float32(0)
	var pullToRefreshLimitPositionPx	: Float32 = Float32(0)


	// Scroll listeners:

	private var onScrollAction : (scrollShiftPx: Int32) -> Unit = {}

	fun setOnScrollAction (
		toCallAtOnce : Boolean = false,
		onScrollAction : (scrollShiftPx: Int32) -> Unit
	) {
		this.onScrollAction = onScrollAction
		if (toCallAtOnce) {
			onScrollAction(0)
		}
	}

	private var onScrollReleaseAction : () -> Unit = {}
	fun setOnScrollReleaseAction (onScrollReleaseAction: () -> Unit) { this.onScrollReleaseAction = onScrollReleaseAction }


	// Swipe to refresh listeners:

	private var onSwipeToRefreshPositionChangedAction : (swipeToRefreshPositionPx: Float32) -> Unit = {}
	fun setOnSwipeToRefreshPositionChangedAction (
		onSwipeToRefreshPositionChangedAction : (swipeToRefreshPositionPx: Float32) -> Unit
	) {
		this.onSwipeToRefreshPositionChangedAction = onSwipeToRefreshPositionChangedAction
	}

	private var onSwipeToRefreshAction : () -> Unit = {}
	fun setOnPullToRefreshAction (onSwipeToRefreshAction : () -> Unit) {
		this.onSwipeToRefreshAction = onSwipeToRefreshAction
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}