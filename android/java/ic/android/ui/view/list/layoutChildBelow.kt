@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.android.ui.view.list.VerticalRecyclerListView
import ic.base.primitives.int32.Int32
import ic.struct.list.ext.get


internal fun <Item: Any> VerticalRecyclerListView<Item>.layoutChildBelow (

	widthPx : Int,
	heightPx : Int,

	itemIndex : Int,

	positionPx : Int32

) : Int {

	val itemController = recycler[itemsField[itemIndex]]

	val viewHeightPx = itemController.view.measuredHeight

	layoutChild(
		widthPx = widthPx,
		heightPx = heightPx,
		itemController = itemController,
		viewHeightPx = viewHeightPx,
		viewPositionPx = positionPx
	)

	return viewHeightPx

}