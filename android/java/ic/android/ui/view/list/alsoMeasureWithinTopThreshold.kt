@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.android.ui.view.list.VerticalRecyclerListView
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


internal fun <Item: Any> VerticalRecyclerListView<Item>.alsoMeasureWithinTopThreshold (

	topVisibilityThresholdPx : Float32,

	firstVisibleWithinPaddingItemIndex : Int,
	firstVisibleWithinPaddingItemPositionPx : Int32

) {

	var itemIndex : Int = firstVisibleWithinPaddingItemIndex
	var itemPositionPx : Int32 = firstVisibleWithinPaddingItemPositionPx

	while (itemIndex > 0 && itemPositionPx >= -topVisibilityThresholdPx) {
		itemIndex--
		val childHeight = measureChild(itemIndex)
		itemPositionPx -= childHeight
	}

	firstVisibleWithinThresholdItemIndex = itemIndex

}