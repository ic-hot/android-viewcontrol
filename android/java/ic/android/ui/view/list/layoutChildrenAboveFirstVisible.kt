@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.list.VerticalRecyclerListView


internal fun <Item: Any> VerticalRecyclerListView<Item>.layoutChildrenAboveFirstVisible (

	widthPx : Int,
	heightPx : Int

) {

	var itemPositionPx = topPaddingPx + firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
	var itemIndex = firstVisibleWithinPaddingItemIndexField - 1

	while (itemIndex >= firstVisibleWithinThresholdItemIndex) {

		val childHeight = layoutChildAbove(
			widthPx = widthPx,
			heightPx = heightPx,
			itemIndex = itemIndex,
			positionPx = itemPositionPx
		)

		itemIndex--
		itemPositionPx -= childHeight

	}

}