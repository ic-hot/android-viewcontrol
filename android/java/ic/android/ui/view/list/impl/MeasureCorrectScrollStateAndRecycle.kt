@file:Suppress("DEPRECATION")


package ic.android.ui.view.list.impl


import ic.android.ui.view.ext.bottomPaddingPx
import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.list.*
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.asInt32
import ic.struct.list.ext.atLeastOneInRange


internal fun VerticalListView.measureCorrectScrollStateAndRecycle (

	topVisibilityThresholdPx 	: Float32,
	bottomVisibilityThresholdPx : Float32

) {

	val heightPx = measuredHeight

	val itemsCount = itemsField.length.asInt32

	var itemPositionPx = topPaddingPx + firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
	var itemIndex = firstVisibleWithinPaddingItemIndexField

	// Normal measuring:
	while (itemIndex < itemsCount && itemPositionPx < heightPx - bottomPaddingPx) {
		val childHeight = measureChild(itemIndex)
		itemIndex++
		itemPositionPx += childHeight
		if (itemPositionPx <= topPaddingPx) {
			firstVisibleWithinPaddingItemIndexField = itemIndex
			firstVisibleWithinPaddingItemPositionRelativeToPaddingPx = itemPositionPx - topPaddingPx
		}
	}

	// Scroll correction by bottom:
	if (itemIndex == itemsCount && itemPositionPx < heightPx - bottomPaddingPx) {
		val scrollCorrectionPx = -(heightPx - bottomPaddingPx - itemPositionPx)
		firstVisibleWithinPaddingItemPositionRelativeToPaddingPx -= scrollCorrectionPx
		continuousScrollPositionPxField += scrollCorrectionPx
	}

	// Scroll correction by top:
	while (firstVisibleWithinPaddingItemPositionRelativeToPaddingPx > 0) {
		if (firstVisibleWithinPaddingItemIndexField == 0) {
			if (firstVisibleWithinPaddingItemPositionRelativeToPaddingPx > 0) {
				val scrollCorrectionPx = firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
				firstVisibleWithinPaddingItemPositionRelativeToPaddingPx = 0
				continuousScrollPositionPxField += scrollCorrectionPx
				itemPositionPx -= scrollCorrectionPx
			}
			break
		}
		firstVisibleWithinPaddingItemIndexField--
		firstVisibleWithinPaddingItemPositionRelativeToPaddingPx -= measureChild(firstVisibleWithinPaddingItemIndexField)
	}

	// Continue measuring after correction by top:
	while (
		itemIndex < itemsCount &&
		itemPositionPx < heightPx - bottomPaddingPx
	) {
		val childHeight = measureChild(itemIndex)
		itemIndex++
		itemPositionPx += childHeight
	}

	alsoMeasureWithinTopThreshold(
		topVisibilityThresholdPx = topVisibilityThresholdPx,
		firstVisibleWithinPaddingItemIndex = firstVisibleWithinPaddingItemIndexField,
		firstVisibleWithinPaddingItemPositionPx = topPaddingPx + firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
	)

	alsoMeasureWithinBottomThreshold(
		bottomVisibilityThresholdPx = bottomVisibilityThresholdPx,
		itemsCount = itemsCount,
		heightPx = heightPx,
		firstInvisibleWithinPaddingItemIndex = itemIndex,
		firstInvisibleWithinPaddingItemPositionPx = itemPositionPx
	)

	// Cleaning up:
	recycler.recycle { item ->
		itemsField.atLeastOneInRange(
			firstVisibleWithinThresholdItemIndex, firstInvisibleWithinThresholdItemIndex
		) { it == item }
	}

}