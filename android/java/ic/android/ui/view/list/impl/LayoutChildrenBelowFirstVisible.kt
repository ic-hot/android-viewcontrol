@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.impl


import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.list.VerticalListView


internal fun VerticalListView.layoutChildrenBelowFirstVisible (

	widthPx : Int,
	heightPx : Int

) {

	var itemPositionPx = topPaddingPx + firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
	var itemIndex = firstVisibleWithinPaddingItemIndexField

	while (itemIndex < firstInvisibleWithinThresholdItemIndex) {

		val childHeight = layoutChildBelow(
			widthPx = widthPx,
			heightPx = heightPx,
			itemIndex = itemIndex,
			positionPx = itemPositionPx
		)

		itemIndex++
		itemPositionPx += childHeight

	}

}