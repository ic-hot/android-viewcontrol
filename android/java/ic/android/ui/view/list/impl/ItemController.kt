package ic.android.ui.view.list


import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewController


internal typealias ItemController<Item> = GenerativeSetStateViewController<Item>