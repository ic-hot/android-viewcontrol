@file:Suppress("NOTHING_TO_INLINE", "DEPRECATION")


package ic.android.ui.view.list.impl


import ic.base.primitives.int32.Int32
import ic.ifaces.indexable.int32.SetInt32Index

import ic.android.ui.view.list.VerticalListView
import ic.struct.list.ext.get


internal inline fun VerticalListView.getEntry (

	itemIndex : Int32,

	toApplyState : Boolean

) : VerticalListView.Entry {

	val item = itemsField[itemIndex]

	val entry = recycler[item]

	if (toApplyState) {

		val itemViewController = entry.itemController

		if (itemViewController is SetInt32Index) {
			if (itemViewController.index != itemIndex) {
				itemViewController.index = itemIndex
			}
		}

	}

	return entry

}