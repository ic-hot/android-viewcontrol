package ic.android.ui.view.list.impl


import ic.base.primitives.int32.Int32

import ic.android.ui.view.list.VerticalListView


internal fun VerticalListView.layoutChildBelow (

	widthPx : Int,
	heightPx : Int,

	itemIndex : Int,

	positionPx : Int32

) : Int {

	val itemController = getEntry(itemIndex = itemIndex, toApplyState = false)

	val viewHeightPx = itemController.view.measuredHeight

	layoutChild(
		widthPx = widthPx,
		heightPx = heightPx,
		entry = itemController,
		viewHeightPx = viewHeightPx,
		viewPositionPx = positionPx
	)

	return viewHeightPx

}