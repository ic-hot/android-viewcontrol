@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.impl


import ic.base.primitives.int32.Int32

import android.view.View

import ic.android.ui.view.list.VerticalListView
import ic.ifaces.indexable.int32.SetInt32Index


private fun VerticalListView.measureChild (child: View) : Int {
	val widthPx = measuredWidth
	child.measure(
		View.MeasureSpec.makeMeasureSpec(widthPx, View.MeasureSpec.EXACTLY),
		View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
	)
	return child.measuredHeight
}


internal fun VerticalListView.measureChild (
	itemIndex : Int32
) : Int32 {
	val entry = getEntry(itemIndex = itemIndex, toApplyState = true)
	val itemViewController = entry.itemController
	if (itemViewController is SetInt32Index) {
		itemViewController.index = itemIndex
	}
	return measureChild(entry.view)
}