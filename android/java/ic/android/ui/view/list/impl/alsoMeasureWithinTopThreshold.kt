package ic.android.ui.view.list.impl


import ic.base.primitives.float32.Float32

import ic.android.ui.view.list.VerticalListView
import ic.base.primitives.int32.Int32


internal fun VerticalListView.alsoMeasureWithinTopThreshold (

	topVisibilityThresholdPx : Float32,

	firstVisibleWithinPaddingItemIndex : Int,
	firstVisibleWithinPaddingItemPositionPx : Int32

) {

	var itemIndex : Int = firstVisibleWithinPaddingItemIndex
	var itemPositionPx : Int32 = firstVisibleWithinPaddingItemPositionPx

	while (itemIndex > 0 && itemPositionPx >= -topVisibilityThresholdPx) {
		itemIndex--
		val childHeight = measureChild(itemIndex)
		itemPositionPx -= childHeight
	}

	firstVisibleWithinThresholdItemIndex = itemIndex

}