package ic.android.ui.view.list.impl


import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.list.VerticalListView


internal fun VerticalListView.layoutChildrenAboveFirstVisible (

	widthPx : Int,
	heightPx : Int

) {

	var itemPositionPx = topPaddingPx + firstVisibleWithinPaddingItemPositionRelativeToPaddingPx
	var itemIndex = firstVisibleWithinPaddingItemIndexField - 1

	while (itemIndex >= firstVisibleWithinThresholdItemIndex) {

		val childHeight = layoutChildAbove(
			widthPx = widthPx,
			heightPx = heightPx,
			itemIndex = itemIndex,
			positionPx = itemPositionPx
		)

		itemIndex--
		itemPositionPx -= childHeight

	}

}