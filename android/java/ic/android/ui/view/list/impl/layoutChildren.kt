package ic.android.ui.view.list.impl


import ic.android.ui.view.list.VerticalListView


internal fun VerticalListView.layoutChildren (

	widthPx : Int,
	heightPx : Int

) {

	layoutChildrenAboveFirstVisible(
		widthPx = widthPx,
		heightPx = heightPx
	)

	layoutChildrenBelowFirstVisible(
		widthPx = widthPx,
		heightPx = heightPx
	)

}