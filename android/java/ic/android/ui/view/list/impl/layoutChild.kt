package ic.android.ui.view.list.impl


import ic.base.primitives.float32.Float32

import ic.android.ui.view.control.list.HandlingTranslationZ
import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.ext.translationZPx
import ic.android.ui.view.list.VerticalListView
import ic.base.primitives.int32.Int32
import ic.math.ext.clamp
import ic.math.normalize


internal fun VerticalListView.layoutChild (

	widthPx : Int,
	heightPx : Int,

	entry : VerticalListView.Entry,

	viewHeightPx : Int32,

	viewPositionPx : Int32

) {

	entry.view.layout(
		0, viewPositionPx,
		widthPx,
		viewPositionPx + viewHeightPx
	)

	val itemPositionRelativeToPaddingPx = viewPositionPx - topPaddingPx
	val pullToRefreshItemTranslationPx = pullToRefreshPositionPxField * (
		1 - (
			itemPositionRelativeToPaddingPx
			.normalize(0, heightPx - topPaddingPx)
			.normalize(0, Float32(.5))
			.clamp()
		)
	)
	entry.view.translationY = pullToRefreshItemTranslationPx
	entry.view.translationZPx = pullToRefreshItemTranslationPx

	val itemViewController = entry.itemController

	if (itemViewController is HandlingTranslationZ) {
		itemViewController.setTranslationZPx(pullToRefreshItemTranslationPx / 4)
	}

}