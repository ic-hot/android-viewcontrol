package ic.android.ui.view.list.impl


import ic.base.primitives.int32.Int32

import ic.android.ui.view.list.VerticalListView


internal fun VerticalListView.layoutChildAbove (

	widthPx : Int,
	heightPx : Int,

	itemIndex : Int,

	positionPx : Int32

) : Int {

	val entry = getEntry(itemIndex = itemIndex, toApplyState = false)

	val viewHeightPx = entry.view.measuredHeight

	layoutChild(
		widthPx = widthPx,
		heightPx = heightPx,
		entry = entry,
		viewHeightPx = viewHeightPx,
		viewPositionPx = positionPx - viewHeightPx
	)

	return viewHeightPx

}