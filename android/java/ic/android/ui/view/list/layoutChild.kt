@file:Suppress("DEPRECATION")


package ic.android.ui.view.list.deprecated


import ic.math.ext.clamp
import ic.math.normalize

import ic.android.ui.view.ext.topPaddingPx
import ic.android.ui.view.ext.translationZPx
import ic.android.ui.view.list.VerticalRecyclerListView
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


internal fun <Item: Any> VerticalRecyclerListView<Item>.layoutChild (

	widthPx : Int,
	heightPx : Int,

	itemController : VerticalRecyclerListView<Item>.ItemController<*>,

	viewHeightPx : Int,

	viewPositionPx : Int32

) {

	itemController.view.layout(
		0, viewPositionPx,
		widthPx,
		viewPositionPx + viewHeightPx
	)

	val itemPositionRelativeToPaddingPx = viewPositionPx - topPaddingPx
	val pullToRefreshItemTranslationPx = pullToRefreshPositionPxField * (
		1 - itemPositionRelativeToPaddingPx.normalize(0, heightPx - topPaddingPx).normalize(0, Float32(.5)).clamp()
	)
	itemController.view.translationY = pullToRefreshItemTranslationPx
	itemController.view.translationZPx = pullToRefreshItemTranslationPx
	itemController.setTranslationZPx(pullToRefreshItemTranslationPx / 4)

}