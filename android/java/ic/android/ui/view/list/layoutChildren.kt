@file:Suppress("DEPRECATION")

package ic.android.ui.view.list.deprecated


import ic.android.ui.view.list.VerticalRecyclerListView


internal fun <Item: Any> VerticalRecyclerListView<Item>.layoutChildren (

	widthPx : Int,
	heightPx : Int

) {

	layoutChildrenAboveFirstVisible(
		widthPx = widthPx,
		heightPx = heightPx
	)

	layoutChildrenBelowFirstVisible(
		widthPx = widthPx,
		heightPx = heightPx
	)

}