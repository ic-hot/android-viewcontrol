@file:Suppress("DEPRECATION")


package ic.android.ui.view.list.ext


import ic.android.ui.view.list.ItemController
import ic.android.ui.view.list.VerticalRecyclerListView
import ic.android.ui.view.list.VerticalListView
import ic.ifaces.getter.Getter
import ic.design.control.list.ListController


inline fun <Item: Any> VerticalRecyclerListView<Item>.open (

	crossinline generateItemController : () -> VerticalRecyclerListView<Item>.ItemController<*>

) {

	open(
		Getter(generateItemController)
	)

}


inline fun <Item> VerticalListView.open (

	crossinline generateItemController : () -> ItemController<Item>

) : ListController<Item> {

	return open(
		Getter(generateItemController)
	)

}
