package ic.android.ui.view.list.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.math.ext.clamp

import ic.android.ui.view.ext.setOnLayoutChangedAction
import ic.android.ui.view.list.VerticalListView
import ic.android.util.units.dpToPx


fun VerticalListView.setDecorElevationHandler (

	maxTopBarElevationPx : Int32,

	setTopBarElevationPx : (Int32) -> Unit = {},

	maxBottomBarElevationPx : Int32,

	setBottomBarElevationPx : (Int32) -> Unit = {}

) {

	fun updateElevations() {

		setTopBarElevationPx(
			(-firstItemPositionRelativeToPaddingPx).clamp(0, maxTopBarElevationPx)
		)

		setBottomBarElevationPx(0)

	}

	setOnScrollAction {
		updateElevations()
	}

	setOnLayoutChangedAction {
		updateElevations()
	}

}


fun VerticalListView.setDecorElevationHandler (

	maxTopBarElevationDp : Float32 = Float32(8),

	setTopBarElevationPx : (Int32) -> Unit = {},

	maxBottomBarElevationDp : Float32 = Float32(16),

	setBottomBarElevationPx : (Int32) -> Unit = {}

) = setDecorElevationHandler(
	maxTopBarElevationPx = dpToPx(maxTopBarElevationDp),
	setTopBarElevationPx = setTopBarElevationPx,
	maxBottomBarElevationPx = dpToPx(maxBottomBarElevationDp),
	setBottomBarElevationPx = setBottomBarElevationPx
)