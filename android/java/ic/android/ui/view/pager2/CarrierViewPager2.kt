@file:Suppress("DEPRECATION")

package ic.android.ui.view.pager2


import ic.struct.list.ext.findIndexOrThrowNotExists

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import ic.android.ui.view.pager2.ext.recyclerView

import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.asInt32
import ic.base.throwables.NotExistsException
import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.get


@Deprecated("")
abstract class CarrierViewPager2<Item: Any, ItemType: Any> : FrameLayout {


	private val environment = ic.android.ui.viewcarrier.ViewCarrier.Environment(context as Activity, this)


	val asViewPager2 = ViewPager2(context).apply {
		offscreenPageLimit = 1
		clipChildren = false
		clipToPadding = false
		recyclerView.apply {
			clipChildren = false
			clipToPadding = false
		}
	}

	init {
		@Suppress("LeakingThis")
		addView(asViewPager2, LayoutParams(MATCH_PARENT, MATCH_PARENT))
		overScrollMode = OVER_SCROLL_NEVER
	}


	protected abstract fun generateItemViewCarrier (itemType: ItemType) : GenerativeCarrier<out View, out Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>

	protected abstract fun getItemType (item: Item) : ItemType


	private var itemsValue : List<Item> = List()


	private val itemTypes = EditableList<ItemType>()


	private class CarrierViewHolder<Item: Any> (

		val viewCarrier : GenerativeCarrier<out View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>,

		itemView : View

	) : RecyclerView.ViewHolder(itemView)


	init {

		asViewPager2.adapter = object : RecyclerView.Adapter<CarrierViewHolder<Item>>() {
			override fun getItemCount() = itemsValue.length.asInt32
			override fun getItemViewType (position: Int) : Int {
				val itemType = getItemType(itemsValue[position])
				return try {
					itemTypes.findIndexOrThrowNotExists { it == itemType }.asInt32
				} catch (notExists: NotExistsException) {
					itemTypes.add(itemType)
					itemTypes.length.asInt32 - 1
				}
			}
			override fun onCreateViewHolder (parent: ViewGroup, itemTypeIndex: Int) : CarrierViewHolder<Item> {
				@Suppress("UNCHECKED_CAST")
				val itemViewCarrier = generateItemViewCarrier(itemTypes[itemTypeIndex])
					as GenerativeCarrier<out View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>
				;
				return CarrierViewHolder(
					viewCarrier = itemViewCarrier,
					itemView = itemViewCarrier.open(environment).apply {
						layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
					}
				)
			}
			override fun onBindViewHolder (holder: CarrierViewHolder<Item>, position: Int) {
				holder.viewCarrier.initState(itemsValue[position])
			}
		}

	}


	protected val items : List<Item> get() = itemsValue

	fun setItems (items : List<Item>) {
		itemsValue = items
		asViewPager2.adapter!!.notifyDataSetChanged()
	}

	fun updateItems() = setItems(items)


	val itemsCount : Int get() = itemsValue.length.asInt32

	@Suppress("UNUSED_PARAMETER")
	fun setCurrentItemIndex (index: Int, toAnimate: Boolean) {
		asViewPager2.currentItem = index
	}

	var currentItemIndex : Int
		get() = asViewPager2.currentItem
		set(value) {
			setCurrentItemIndex(value, true)
		}
	;

	val currentItem : Item get() = items[currentItemIndex]


	private var onCurrentItemChangedAction : ((itemIndex: Int, Item) -> Unit)? = null

	fun setOnCurrentItemChangedAction (
		toCallAtOnce: Boolean = false,
		onCurrentItemChangedAction: (currentItemIndex: Int, Item) -> Unit
	) {
		this.onCurrentItemChangedAction = onCurrentItemChangedAction
		if (toCallAtOnce) onCurrentItemChangedAction(currentItemIndex, currentItem)
	}


	init {
		asViewPager2.registerOnPageChangeCallback(
			object : ViewPager2.OnPageChangeCallback() {
				override fun onPageScrolled (position: Int, positionOffset: Float, positionOffsetPixels: Int) {
					fun onPageScroll (position: Int, scrollPhase: Float32) {
						if (position < 0) 									return
						if (position >= asViewPager2.adapter!!.itemCount) 	return
						@Suppress("UNCHECKED_CAST")
						val viewHolder = (
							asViewPager2.recyclerView.findViewHolderForAdapterPosition(position) as CarrierViewHolder<Item>?
						)
						if (viewHolder == null) return
						val viewCarrier = viewHolder.viewCarrier
						if (viewCarrier is ic.android.ui.viewcarrier.PageViewCarrier) {
							viewCarrier.onPageScroll(scrollPhase)
						}
					}
					onPageScroll (position - 1, 	-positionOffset - 1	)
					onPageScroll (position, 		-positionOffset		)
					onPageScroll (position + 1, 	-positionOffset + 1	)
				}
				override fun onPageSelected (position: Int) {
					onCurrentItemChangedAction?.invoke(position, items[position])
				}
			}
		)
	}


	fun close() {
		currentItemIndex = 0
		setItems(List())
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


	abstract class NoTypes<Item: Any> : CarrierViewPager2<Item, Unit> {

		@Suppress("ConvertSecondaryConstructorToPrimary")
		@JvmOverloads
		constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
			: super(context, attrs, defStyleAttr)
		;

		override fun getItemType (item: Item) = Unit

	}


}