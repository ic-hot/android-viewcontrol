package ic.android.ui.view.pager2.ext


import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

import ic.android.ui.view.group.ext.get


inline val ViewPager2.recyclerView : RecyclerView get() = this[0]