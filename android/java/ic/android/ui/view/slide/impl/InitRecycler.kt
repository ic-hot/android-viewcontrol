package ic.android.ui.view.slide.impl


import ic.android.ui.view.slide.HorizontalSlideView
import ic.ifaces.stateful.ext.state
import ic.util.recycler.Recycler


internal fun HorizontalSlideView.initRecycler() = Recycler(

	generateValue = {
		val itemController = generateItemController()
		val itemView = itemController.open(
			environment = itemAndroidViewScope
		)
		Entry(
			itemController = itemController,
			itemView = itemView
		)
	},

	onSeize = { item: Any?, entry ->
		entry.itemController.state = item
	},

	onRelease = {},

	onClose = { entry ->
		entry.itemController.close()
	}

)