package ic.android.ui.view.slide.impl


import android.view.View

import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewController


internal class Entry (

	val itemController : GenerativeSetStateViewController<*>,

	val itemView : View

)