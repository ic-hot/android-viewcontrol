@file:Suppress("DEPRECATION")

package ic.android.ui.view.slide.ext



import android.view.View

import ic.ifaces.getter.Getter

import ic.android.ui.view.slide.HorizontalSlideRecyclerListView
import ic.pattern.carrier.GenerativeCarrier


inline fun <Item: Any> HorizontalSlideRecyclerListView<Item>.open (

	crossinline generateItemViewCarrier : () -> GenerativeCarrier<View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>

) {

	open(Getter(generateItemViewCarrier))

}