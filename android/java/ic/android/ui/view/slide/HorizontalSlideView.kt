@file:Suppress("LeakingThis")


package ic.android.ui.view.slide


import kotlin.math.roundToInt

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.ifaces.cancelable.Cancelable
import ic.math.funs.min
import ic.struct.set.editable.EditableSet
import ic.android.ui.touch.TouchEvent
import ic.android.ui.view.group.ViewGroup
import ic.android.ui.touch.drag.HorizontalDragHandler
import ic.android.ui.touch.view.dispatchViewTouchEvent
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewController
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.ui.view.ext.measureInPx
import ic.android.ui.view.slide.impl.initRecycler
import ic.android.util.units.dpToPx
import ic.android.util.units.pxToDp
import ic.gui.anim.dynamic.DynamicValueAnimatorForSwipeDp
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int64.asInt32
import ic.ifaces.scrollable.phase.PhaseScrollable
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.ext.clamp
import ic.math.funs.max
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.List
import ic.struct.list.ext.atLeastOneInRange
import ic.struct.list.ext.findIndexOrNull
import ic.struct.list.ext.get
import ic.struct.list.ext.getOrNull
import ic.struct.list.ext.length.isNotEmpty


class HorizontalSlideView : ViewGroup {


	internal lateinit var generateItemController : () -> GenerativeSetStateViewController<Any?>

	fun open (generateItemController : () -> GenerativeSetStateViewController<*>) {
		@Suppress("UNCHECKED_CAST")
		this.generateItemController = (
			generateItemController as () -> GenerativeSetStateViewController<Any?>
		)
	}


	internal val itemAndroidViewScope = AndroidViewScope(context = context, parentView = this)


	private val recycler = initRecycler()


	// State:

	private var items : List<Any?> = List()

	private var firstVisibleItemIndex : Int = 0

	private var firstVisibleItemPositionPx : Int32 = 0


	private fun correctScrollStateAndRecycle() {

		val widthPx = measuredWidth

		while (firstVisibleItemPositionPx <= -widthPx) {
			firstVisibleItemIndex++
			firstVisibleItemPositionPx += widthPx
		}
		while (firstVisibleItemPositionPx > 0) {
			firstVisibleItemIndex--
			firstVisibleItemPositionPx -= widthPx
		}
		if (firstVisibleItemIndex >= items.length) {
			firstVisibleItemIndex = items.length.asInt32 - 1
			firstVisibleItemPositionPx = 0
		}
		if (firstVisibleItemIndex < 0) {
			firstVisibleItemIndex = 0
			firstVisibleItemPositionPx = 0
		}
		if (firstVisibleItemIndex == items.length.asInt32 - 1) {
			firstVisibleItemPositionPx = 0
		}

		recycler.recycle { item ->
			items.atLeastOneInRange(
				max(0, firstVisibleItemIndex - 1),
				min(items.length.asInt32, firstVisibleItemIndex + 2)
			) { it == item }
		}

	}

	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {

		val widthPx 	= MeasureSpec.getSize(widthMeasureSpec)
		val heightPx 	= MeasureSpec.getSize(heightMeasureSpec)
		setMeasuredDimension(widthPx, heightPx)

		removeAllViewsInLayout()

		correctScrollStateAndRecycle()

		if (items.isNotEmpty) {
			val view = recycler[items[firstVisibleItemIndex]].itemView
			view.measureInPx(widthPx, heightPx)
			addViewInLayout(view, childCount, view.layoutParams)
		}
		if (firstVisibleItemIndex > 0) {
			val view = recycler[items[firstVisibleItemIndex - 1]].itemView
			view.measureInPx(widthPx, heightPx)
			addViewInLayout(view, childCount, view.layoutParams)
		}
		if (firstVisibleItemIndex < items.length - 1) {
			val view = recycler[items[firstVisibleItemIndex + 1]].itemView
			view.measureInPx(widthPx, heightPx)
			addViewInLayout(view, childCount, view.layoutParams)
		}
		if (firstVisibleItemIndex < items.length - 2) {
			val view = recycler[items[firstVisibleItemIndex + 2]].itemView
			view.measureInPx(widthPx, heightPx)
			addViewInLayout(view, childCount, view.layoutParams)
		}

	}

	override fun onLayout (changed: Boolean, l: Int, t: Int, r: Int, b: Int) {

		val widthPx 	= right - left
		val heightPx 	= bottom - top

		if (items.isNotEmpty) {
			val view = recycler[items[firstVisibleItemIndex]].itemView
			view.layout(
				firstVisibleItemPositionPx, 0,
				firstVisibleItemPositionPx + widthPx, heightPx
			)
		}
		if (firstVisibleItemIndex > 0) {
			val view = recycler[items[firstVisibleItemIndex - 1]].itemView
			view.layout(
				firstVisibleItemPositionPx - widthPx, 0,
				firstVisibleItemPositionPx - widthPx + widthPx, heightPx
			)
		}
		if (firstVisibleItemIndex < items.length - 1) {
			val view = recycler[items[firstVisibleItemIndex + 1]].itemView
			view.layout(
				firstVisibleItemPositionPx + widthPx, 0,
				firstVisibleItemPositionPx + widthPx + widthPx, heightPx
			)
		}
		if (firstVisibleItemIndex < items.length - 2) {
			val view = recycler[items[firstVisibleItemIndex + 2]].itemView
			view.layout(
				firstVisibleItemPositionPx + widthPx + widthPx, 0,
				firstVisibleItemPositionPx + widthPx + widthPx + widthPx, heightPx
			)
		}

	}


	// List actions:

	fun setItems (items: List<*>) {
		val firstVisibleItem = this.items.getOrNull(firstVisibleItemIndex)
		@Suppress("UNCHECKED_CAST")
		this.items = items as List<Any?>
		firstVisibleItemIndex = items.findIndexOrNull { it == firstVisibleItem }?.asInt32 ?: 0
		if (measuredWidth != 0) correctScrollStateAndRecycle()
		invokeOnScrollListeners()
		requestLayout()
	}

	val itemsCount : Int get() = items.length.asInt32


	// Scroll position:

	private val onScrollListeners = EditableSet <
		(firstVisiblePageIndex: Int, scrollOffsetPx: Int32, scrollOffsetPhase: Float32) -> Unit
	> ()

	val scrollOffsetPhase : Float32
		get() {
		if (measuredWidth == 0) return Float32(0)
		return -firstVisibleItemPositionPx.asFloat32 / measuredWidth
	}

	fun listenOnScroll (
		toCallAtOnce : Boolean = false,
		onScrollListener: (firstVisiblePageIndex: Int, scrollOffsetPx: Int32, scrollOffsetPhase: Float32) -> Unit
	) : Cancelable {
		onScrollListeners.add(onScrollListener)
		if (toCallAtOnce) {
			onScrollListener(firstVisibleItemIndex, -firstVisibleItemPositionPx, scrollOffsetPhase)
		}
		return object : Cancelable {
			override fun cancel() {
				onScrollListeners.remove(onScrollListener)
			}
		}
	}

	private fun invokeOnScrollListeners() {
		val phase = if(measuredWidth == 0) 0f else firstVisibleItemPositionPx.asFloat32 / measuredWidth
		if (items.isNotEmpty) {
			val itemController = recycler[items[firstVisibleItemIndex]].itemController
			if (itemController is PhaseScrollable) {
				itemController.setScrollPhase(phase)
			}
		}
		if (firstVisibleItemIndex > 0) {
			val itemController = recycler[items[firstVisibleItemIndex - 1]].itemController
			if (itemController is PhaseScrollable) {
				itemController.setScrollPhase(phase - 1)
			}
		}
		if (firstVisibleItemIndex < items.length - 1) {
			val itemController = recycler[items[firstVisibleItemIndex + 1]].itemController
			if (itemController is PhaseScrollable) {
				itemController.setScrollPhase(phase + 1)
			}
		}
		if (firstVisibleItemIndex < items.length - 2) {
			val itemController = recycler[items[firstVisibleItemIndex + 2]].itemController
			if (itemController is PhaseScrollable) {
				itemController.setScrollPhase(phase + 2)
			}
		}
		onScrollListeners.breakableForEach {
			it(firstVisibleItemIndex, -firstVisibleItemPositionPx, scrollOffsetPhase)
		}
	}

	private val scrollDpAnimator = DynamicValueAnimatorForSwipeDp { scrollPositionDp, scrollVelocityDpPerS ->
		val scrollPositionPx = dpToPx(scrollPositionDp)
		firstVisibleItemIndex = 0
		firstVisibleItemPositionPx = -scrollPositionPx
		correctScrollStateAndRecycle()
		val correctedScrollPositionPx = -firstVisibleItemPositionPx + firstVisibleItemIndex * measuredWidth
		if (correctedScrollPositionPx == scrollPositionPx) {
			requestLayout()
			invokeOnScrollListeners()
		} else {
			val correctedScrollPositionDp = pxToDp(correctedScrollPositionPx)
			correct(
				position = correctedScrollPositionDp,
				velocityInvS = scrollVelocityDpPerS
			)
		}
	}


	// Selected item index:

	private val onSelectedPageChangedListeners = EditableSet<(selectedPageIndex: Int) -> Unit>()

	fun listenOnCurrentPageChanged (
		toCallAtOnce : Boolean = false,
		onSelectedPageChangedListener : (currentPageIndex: Int) -> Unit
	) : Cancelable {
		onSelectedPageChangedListeners.add(onSelectedPageChangedListener)
		if (toCallAtOnce) {
			onSelectedPageChangedListener(currentPageIndex)
		}
		return object : Cancelable {
			override fun cancel() {
				onSelectedPageChangedListeners.remove(onSelectedPageChangedListener)
			}
		}
	}


	var currentPageIndex : Int32 = 0
		set(value) {
			field = value
			var targetPageIndex = value
			if (targetPageIndex > items.count - 1) targetPageIndex = items.count.asInt32 - 1
			if (targetPageIndex < 0) targetPageIndex = 0
			val targetScrollPositionPx = targetPageIndex.asFloat32 * measuredWidth
			val targetScrollPositionDp = pxToDp(targetScrollPositionPx)
			scrollDpAnimator.setEndPosition(targetScrollPositionDp)
			onSelectedPageChangedListeners.breakableForEach { it(value) }
		}
	;


	// Touch handling:

	private sealed class TouchState {
		object Initial 	: TouchState()
		object Scroll 	: TouchState()
	}

	private var scrollPositionAtDownDp : Float32 = Float32(0)

	private var touchState : TouchState = TouchState.Initial

	override val intrinsicTouchHandler = object : HorizontalDragHandler() {
		override fun onDragEvent (dragEvent: TouchEvent) {
			when (dragEvent) {
				is TouchEvent.Down -> {
					scrollDpAnimator.stopNonBlockingIfNeeded()
					scrollPositionAtDownDp = scrollDpAnimator.getPosition()
					touchState = TouchState.Initial
					scrollDpAnimator.seize(
						scrollPositionAtDownDp - dragEvent.relativePositionDp.x
					)
				}
				is TouchEvent.Move -> {
					if (touchState == TouchState.Initial) {
						touchState = TouchState.Scroll
					}
					scrollDpAnimator.move(
						scrollPositionAtDownDp - dragEvent.relativePositionDp.x
					)
				}
				is TouchEvent.Up -> {
					scrollDpAnimator.release()
					var targetScrollPositionDp = scrollDpAnimator.getEndPosition()
					var targetScrollPositionPx = dpToPx(targetScrollPositionDp)
					var targetPageIndex = (targetScrollPositionPx.asFloat32 / measuredWidth).roundToInt().clamp(
						from = max(0, currentPageIndex - 1),
						to = min(itemsCount - 1, currentPageIndex + 1)
					)
					currentPageIndex = targetPageIndex
				}
				is TouchEvent.Cancel -> {
					scrollDpAnimator.stopNonBlockingIfNeeded()
					var targetScrollPositionDp = scrollDpAnimator.getEndPosition()
					var targetScrollPositionPx = dpToPx(targetScrollPositionDp)
					var targetPageIndex = (targetScrollPositionPx.asFloat32 / measuredWidth).roundToInt().clamp(
						from = max(0, currentPageIndex - 1),
						to = min(itemsCount - 1, currentPageIndex + 1)
					)
					currentPageIndex = targetPageIndex
				}
			}
		}
	}

	override fun dispatchTouchEvent (motionEvent: MotionEvent) : Boolean {
		return dispatchViewTouchEvent(
			view = this,
			touchHandler = touchHandler,
			motionEvent = motionEvent,
			superDispatchTouchEvent = { super.dispatchTouchEvent(motionEvent) }
		)
	}


	fun close() {
		setItems(List())
		recycler.close()
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}