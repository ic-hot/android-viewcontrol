package ic.android.ui.view.control


import android.content.Context
import android.view.View

import ic.design.control.BaseStatefulControllerWithEnv


abstract class BaseStatefulViewControllerWithEnv<State, Environment>
	: BaseStatefulControllerWithEnv<View, State, Environment>()
	, StatefulViewControllerWithEnv<State, Environment>
{


	protected abstract val context : Context


}