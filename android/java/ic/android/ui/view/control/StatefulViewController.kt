package ic.android.ui.view.control

import ic.android.ui.view.scope.AndroidViewScope


interface StatefulViewController<State>
	: StatefulViewControllerWithEnv<State, AndroidViewScope>
{



}