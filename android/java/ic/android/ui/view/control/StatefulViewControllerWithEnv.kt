package ic.android.ui.view.control


import ic.design.control.StatefulControllerWithEnv

import android.view.View


interface StatefulViewControllerWithEnv<State, Environment> :
	StatefulControllerWithEnv<View, State, Environment> {



}