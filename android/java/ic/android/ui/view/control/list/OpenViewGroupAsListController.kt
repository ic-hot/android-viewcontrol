package ic.android.ui.view.control.list


import android.content.Context
import android.view.ViewGroup

import ic.design.control.list.ListController

import ic.android.ui.view.group.ext.editableChildren
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewController
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewControllerWithEnv


fun <Item> ViewGroup.openAsListController (

	generateItemViewController : () -> GenerativeSetStateViewController<Item>

) : ListController<Item> {

	val listController = object : RecyclingViewListController<Item>() {

		override fun generateItemController() = generateItemViewController()

	}

	listController.open(
		subject = editableChildren,
		environment = AndroidViewScope(
			context = context,
			parentView = this
		)
	)

	return listController

}


fun <Item, Env: AndroidViewScope> ViewGroup.openAsListController (

	initItemEnvironment : (context: Context, parentView: ViewGroup) -> Env,

	generateItemViewController : () -> GenerativeSetStateViewControllerWithEnv<Item, Env>,

	) : ListController<Item> {

	val listController = object : RecyclingViewListControllerWithEnv<Item, Env>() {

		override fun generateItemController() = generateItemViewController()

	}

	listController.open(
		subject = editableChildren,
		environment = initItemEnvironment(context, this)
	)

	return listController

}