package ic.android.ui.view.control.list


import ic.android.ui.view.scope.AndroidViewScope
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewController


abstract class RecyclingViewListController<Item>
	: RecyclingViewListControllerWithEnv<Item, AndroidViewScope>()
{


	abstract override fun generateItemController() : GenerativeSetStateViewController<Item>


}