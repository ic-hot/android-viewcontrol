package ic.android.ui.view.control.list


import ic.design.control.list.ListController
import ic.util.recycler.list.RecyclingListControllerWithEnv

import android.view.View

import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewControllerWithEnv


abstract class RecyclingViewListControllerWithEnv<Item, Env>
	: RecyclingListControllerWithEnv<View, Item, Env>()
	, ListController<Item>
{


	abstract override fun generateItemController()
		: GenerativeSetStateViewControllerWithEnv<Item, Env>
	;


}