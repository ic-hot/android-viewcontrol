package ic.android.ui.view.control.list


import ic.base.primitives.float32.Float32


interface HandlingTranslationZ {

	fun setTranslationZPx (translationZ: Float32)

}