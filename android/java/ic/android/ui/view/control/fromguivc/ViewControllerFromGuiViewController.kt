package ic.android.ui.view.control.fromguivc


import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout

import ic.gui.control.ViewController
import ic.gui.scope.AndroidGuiScope

import ic.android.ui.view.control.gen.BaseGenerativeViewController


class ViewControllerFromGuiViewController (

	private val viewController : ViewController

) : BaseGenerativeViewController() {


	override fun initSubject() : android.view.View {
		val view = viewController.open(
			AndroidGuiScope(
				context = context
			)
		) as android.view.View
		view.layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
		return view
	}

	override fun onOpen() {
		viewController.updateView()
	}

	override fun onClose() {
		viewController.close()
	}


}