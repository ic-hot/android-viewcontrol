package ic.android.ui.view.control


import ic.android.ui.view.scope.AndroidViewScope


interface SetStateViewController<State>
	: SetStateViewControllerWithEnv<State, AndroidViewScope>
{



}