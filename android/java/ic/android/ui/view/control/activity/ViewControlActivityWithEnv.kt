package ic.android.ui.view.control.activity


import android.os.Bundle
import ic.android.ui.view.scope.AndroidViewScope


abstract class ViewControlActivityWithEnv<Environment: AndroidViewScope>
	: StatefulViewControlActivityWithEnv<Unit, Environment>()
{

	override fun initState() = Unit

	override fun implementParseState (bundle: Bundle) = Unit

	override fun implementSerializeState (state: Unit) = Bundle()

}