package ic.android.ui.view.control.activity


import android.content.Context
import android.view.ViewGroup
import ic.android.ui.view.scope.AndroidViewScope


abstract class StatefulViewControlActivity<State>
	: StatefulViewControlActivityWithEnv<State, AndroidViewScope>()
{

	override fun initEnvironment (context: Context, parentView: ViewGroup?) : AndroidViewScope {
		return AndroidViewScope(
			context = context,
			parentView = parentView
		)
	}

}