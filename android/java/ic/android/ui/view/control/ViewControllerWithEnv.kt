package ic.android.ui.view.control


import ic.design.control.ControllerWithEnv
import ic.android.ui.view.scope.AndroidViewScope

import android.view.View


interface ViewControllerWithEnv<Environment: AndroidViewScope>
	: ControllerWithEnv<View, Environment>
{



}