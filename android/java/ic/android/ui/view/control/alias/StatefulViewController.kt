package ic.android.ui.view.control.alias


import ic.design.control.StatefulControllerWithEnv

import android.view.View
import ic.android.ui.view.scope.AndroidViewScope


typealias StatefulViewController<State>
	= StatefulControllerWithEnv<View, State, AndroidViewScope>
;