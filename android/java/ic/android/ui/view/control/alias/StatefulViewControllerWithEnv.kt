package ic.android.ui.view.control.alias


import ic.design.control.StatefulControllerWithEnv

import android.view.View


typealias StatefulViewControllerWithEnvironment<State, Environment>
	= StatefulControllerWithEnv<View, State, Environment>
;