package ic.android.ui.view.control

import ic.android.ui.view.scope.AndroidViewScope


abstract class BaseStatefulViewController<State>
	: BaseStatefulViewControllerWithEnv<State, AndroidViewScope>()
	, StatefulViewController<State>
{


	override val context get() = environment.context


}