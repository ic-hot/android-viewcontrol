@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.control.ext


import android.content.Context
import android.view.ViewGroup
import ic.android.ui.view.scope.AndroidViewScope
import ic.design.control.gen.GenerativeStatefulControllerWithEnv


inline fun
	<Subject, State>
	GenerativeStatefulControllerWithEnv<Subject, State, AndroidViewScope>
	.openWithState (
		state : State,
		context: Context,
		parentView : ViewGroup? = null
	)
	: Subject
{
	return openWithState(
		state = state,
		environment = AndroidViewScope(
			context = context,
			parentView = parentView
		)
	)
}


inline fun
	<Subject>
	GenerativeStatefulControllerWithEnv<Subject, Unit, AndroidViewScope>
	.openWithState (
		context: Context,
		parentView : ViewGroup? = null
	)
	: Subject
{
	return openWithState(
		state = Unit,
		context = context,
		parentView = parentView
	)
}