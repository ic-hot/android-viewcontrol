@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.control.ext


import android.content.Context
import android.view.ViewGroup
import ic.android.ui.view.scope.AndroidViewScope
import ic.design.control.StatefulControllerWithEnv
import ic.design.control.ext.open
import ic.design.control.gen.GenerativeStatefulControllerWithEnv


inline fun
	<State>
	StatefulControllerWithEnv<Unit, State, AndroidViewScope>
	.open (
		context: Context,
		parentView : ViewGroup? = null
	)
{
	open(
		environment = AndroidViewScope(
			context = context,
			parentView = parentView
		)
	)
}


inline fun
	<Subject, State>
	GenerativeStatefulControllerWithEnv<Subject, State, AndroidViewScope>
	.open (
		context: Context,
		parentView : ViewGroup? = null
	)
	: Subject
{
	return open(
		environment = AndroidViewScope(
			context = context,
			parentView = parentView
		)
	)
}