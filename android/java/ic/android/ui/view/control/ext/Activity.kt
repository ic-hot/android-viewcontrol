package ic.android.ui.view.control.ext


import ic.android.ui.view.scope.AndroidViewScope
import ic.design.control.StatefulControllerWithEnv
import ic.android.ui.view.scope.ext.activity


inline val
	<Env: AndroidViewScope>
	StatefulControllerWithEnv<*, *, Env>.activity
	get() = environment.activity
;
