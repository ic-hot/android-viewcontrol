package ic.android.ui.view.control.ext


import ic.android.ui.view.scope.AndroidViewScope
import ic.design.control.StatefulControllerWithEnv


inline val
	<Env: AndroidViewScope>
	StatefulControllerWithEnv<*, *, Env>.context
	get() = environment.context
;
