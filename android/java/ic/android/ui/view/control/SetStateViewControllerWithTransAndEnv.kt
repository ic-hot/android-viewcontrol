package ic.android.ui.view.control


import ic.design.control.SetStateControllerWithTransAndEnv

import android.view.View


interface SetStateViewControllerWithTransAndEnv<State, Transition, Environment>
	: SetStateControllerWithTransAndEnv<View, State, Transition, Environment>
	, StatefulViewControllerWithEnv<State, Environment>
{



}