@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.control.gen


import ic.android.context.ext.inflateView
import ic.android.ui.view.scope.AndroidViewScope
import ic.base.primitives.int32.Int32
import ic.design.control.gen.BaseGenerativeControllerWithEnv

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup


abstract class BaseGenerativeViewControllerWithEnv<Environment: AndroidViewScope>
	: BaseGenerativeControllerWithEnv<View, Environment>()
{


	protected inline val context : Context get() = environment.context

	protected inline val parentView : ViewGroup? get() = environment.parentView

	protected inline val activity get() = context as Activity

	protected inline fun inflateView (layoutResId: Int32) : View {
		return context.inflateView(
			layoutResId = layoutResId,
			parentView = parentView
		)
	}


}