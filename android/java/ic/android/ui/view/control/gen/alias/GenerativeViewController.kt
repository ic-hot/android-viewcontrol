package ic.android.ui.view.control.gen.alias


import android.view.View
import ic.android.ui.view.scope.AndroidViewScope
import ic.design.control.gen.GenerativeStatefulControllerWithEnv


typealias GenerativeViewController
	= GenerativeStatefulControllerWithEnv<View, Unit, AndroidViewScope>
;