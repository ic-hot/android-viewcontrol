package ic.android.ui.view.control.gen


import ic.design.control.gen.GenerativeSetStateControllerWithTransAndEnv

import android.view.View

import ic.android.ui.view.control.SetStateViewControllerWithTransAndEnv


interface GenerativeSetStateViewControllerWithTransAndEnv
	<State, Transition, Environment>
	: GenerativeSetStateControllerWithTransAndEnv<View, State, Transition, Environment>
	, SetStateViewControllerWithTransAndEnv<State, Transition, Environment>
{



}