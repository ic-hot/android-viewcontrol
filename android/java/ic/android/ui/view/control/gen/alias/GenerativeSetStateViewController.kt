package ic.android.ui.view.control.gen.alias


import ic.design.control.gen.GenerativeSetStateControllerWithTransAndEnv

import android.view.View
import ic.android.ui.view.scope.AndroidViewScope


typealias GenerativeSetStateViewController<State> =
	GenerativeSetStateControllerWithTransAndEnv<View, State, Unit, AndroidViewScope>
;