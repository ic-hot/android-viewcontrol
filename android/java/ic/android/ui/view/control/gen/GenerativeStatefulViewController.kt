package ic.android.ui.view.control.gen


import ic.android.ui.view.control.StatefulViewController
import ic.android.ui.view.scope.AndroidViewScope


interface GenerativeStatefulViewController<State>
	: GenerativeStatefulViewControllerWithEnv<State, AndroidViewScope>
	, StatefulViewController<State>
{



}