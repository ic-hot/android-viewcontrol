package ic.android.ui.view.control.gen.alias


import ic.android.ui.view.scope.AndroidViewScope


typealias GenerativeStatefulViewController<State> = GenerativeStatefulViewControllerWithEnv<State, AndroidViewScope>