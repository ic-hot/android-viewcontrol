package ic.android.ui.view.control.gen


import ic.android.ui.view.control.ViewController
import ic.android.ui.view.scope.AndroidViewScope


interface GenerativeViewController
	: GenerativeViewControllerWithEnv<AndroidViewScope>
	, ViewController
{



}