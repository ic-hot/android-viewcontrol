package ic.android.ui.view.control.gen


import ic.ifaces.stateful.ext.stateVal
import ic.struct.value.Val

import ic.gui.scope.AndroidGuiScope
import ic.gui.scope.GuiScope
import ic.gui.view.View


inline fun <State> GenerativeSetStateViewController (

	crossinline initView : GuiScope.(Val<State>) -> View

) : GenerativeSetStateViewController<State> {

	return object : BaseGenerativeSetStateViewController<State>() {

		override fun initSubject() : android.view.View {
			val guiContext = AndroidGuiScope(context)
			return guiContext.initView(stateVal) as android.view.View
		}

		override fun onSetState (state: State) {
			(subject as View).update()
		}

	}

}