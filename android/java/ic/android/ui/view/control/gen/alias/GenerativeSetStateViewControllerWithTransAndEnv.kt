package ic.android.ui.view.control.gen.alias


import ic.design.control.gen.GenerativeSetStateControllerWithTransAndEnv

import android.view.View


typealias GenerativeSetStateViewControllerWithTransAndEnv
	<State, Transition, Environment>
	= GenerativeSetStateControllerWithTransAndEnv<View, State, Transition, Environment>
;