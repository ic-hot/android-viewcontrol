package ic.android.ui.view.control.gen


import android.view.View

import ic.android.ui.view.control.ViewControllerWithEnv
import ic.android.ui.view.scope.AndroidViewScope

import ic.design.control.gen.GenerativeControllerWithEnv


interface GenerativeViewControllerWithEnv<Environment: AndroidViewScope>
	: GenerativeControllerWithEnv<View, Environment>
	, ViewControllerWithEnv<Environment>
{



}