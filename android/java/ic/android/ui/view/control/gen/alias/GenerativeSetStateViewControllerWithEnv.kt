package ic.android.ui.view.control.gen.alias


import ic.design.control.gen.GenerativeSetStateControllerWithTransAndEnv

import android.view.View


typealias GenerativeSetStateViewControllerWithEnv<State, Env> =
	GenerativeSetStateControllerWithTransAndEnv<View, State, Unit, Env>
;