@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.control.gen


import ic.android.ui.view.scope.AndroidViewScope


abstract class BaseGenerativeSetStateViewController<State>
	: BaseGenerativeSetStateViewControllerWithEnv<State, AndroidViewScope>()
	, GenerativeSetStateViewController<State>
{



}