package ic.android.ui.view.control.gen.alias


import ic.design.control.gen.GenerativeStatefulControllerWithEnv

import android.view.View


typealias GenerativeStatefulViewControllerWithEnv<State, Environment>
	= GenerativeStatefulControllerWithEnv<View, State, Environment>
;