package ic.android.ui.view.control.gen


import ic.android.ui.view.scope.AndroidViewScope


interface GenerativeSetStateViewController<State>
	: GenerativeSetStateViewControllerWithEnv<State, AndroidViewScope>
{



}