package ic.android.ui.view.control.gen


import ic.design.control.gen.GenerativeStatefulControllerWithEnv

import android.view.View

import ic.android.ui.view.control.StatefulViewControllerWithEnv


interface GenerativeStatefulViewControllerWithEnv<State, Environment>
	: GenerativeStatefulControllerWithEnv<View, State, Environment>
	, StatefulViewControllerWithEnv<State, Environment>
{



}