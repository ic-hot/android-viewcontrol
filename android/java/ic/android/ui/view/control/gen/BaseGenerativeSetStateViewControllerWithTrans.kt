@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.control.gen


import ic.android.ui.view.scope.AndroidViewScope


abstract class BaseGenerativeSetStateViewControllerWithTrans<State, Transition>
	: BaseGenerativeSetStateViewControllerWithTransAndEnv<State, Transition, AndroidViewScope>()
{



}