package ic.android.ui.view.control.gen


import android.view.View

import ic.design.control.gen.GenerativeSetStateControllerWithEnv


interface GenerativeSetStateViewControllerWithEnv<State, Env>
	: GenerativeSetStateViewControllerWithTransAndEnv<State, Unit, Env>
	, GenerativeSetStateControllerWithEnv<View, State, Env>
{



}