@file:Suppress("DEPRECATION")

package ic.android.ui.view.recycler


import android.app.Activity

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import ic.base.primitives.int64.asInt32
import ic.base.throwables.NotExistsException
import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.findIndexOrThrowNotExists
import ic.struct.list.ext.get


@Suppress("LeakingThis")


@Deprecated("")
abstract class CarrierRecyclerView<Item: Any, ItemType: Any> : RecyclerView {


	private val environment = ic.android.ui.viewcarrier.ViewCarrier.Environment(context as Activity, this)


	protected abstract val isFixedSize : Boolean

	init {
		setHasFixedSize(isFixedSize)
	}


	protected abstract fun initLayoutManager() : LayoutManager

	init {
		layoutManager = initLayoutManager()
	}


	protected abstract fun generateItemViewCarrier (itemType: ItemType) : GenerativeCarrier<out View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>

	protected abstract fun getItemType (item: Item) : ItemType


	private var itemsValue : List<Item> = List()


	private val itemTypes = EditableList<ItemType>()


	private class CarrierViewHolder<Item: Any> (

		val viewCarrier : GenerativeCarrier<out View, Item, ic.android.ui.viewcarrier.ViewCarrier.Environment, *>,

		itemView : View

	) : ViewHolder (itemView)


	init {
		adapter = object : Adapter<CarrierViewHolder<Item>>() {
			override fun getItemCount() = itemsValue.length.asInt32
			override fun getItemViewType (position: Int) : Int {
				val itemType = getItemType(itemsValue[position])
				return try {
					itemTypes.findIndexOrThrowNotExists { it == itemType }.asInt32
				} catch (notExists: NotExistsException) {
					itemTypes.add(itemType)
					itemTypes.length.asInt32 - 1
				}
			}
			override fun onCreateViewHolder (parent: ViewGroup, itemTypeIndex: Int) : CarrierViewHolder<Item> {
				val itemViewCarrier = generateItemViewCarrier(itemTypes[itemTypeIndex])
				return CarrierViewHolder(
					viewCarrier = itemViewCarrier,
					itemView = itemViewCarrier.open(environment)
				)
			}
			override fun onBindViewHolder (holder: CarrierViewHolder<Item>, position: Int) {
				holder.viewCarrier.initState(itemsValue[position])
			}
		}
	}


	protected val items : List<Item> get() = itemsValue

	fun setItems (items : List<Item>) {
		itemsValue = items
		adapter!!.notifyDataSetChanged()
	}



	@Suppress("ConvertSecondaryConstructorToPrimary") @JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)


	abstract class Vertical<Item: Any, ItemType: Any> : CarrierRecyclerView<Item, ItemType> {

		override fun initLayoutManager() = LinearLayoutManager(context, VERTICAL, false)

		@Suppress("ConvertSecondaryConstructorToPrimary")
		@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)

		abstract class NoTypes<Item: Any> : Vertical<Item, Unit> {

			override fun getItemType (item: Item) = Unit

			@Suppress("ConvertSecondaryConstructorToPrimary")
			@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)

		}

	}


	abstract class NoTypes<Item: Any> : CarrierRecyclerView<Item, Unit> {

		override fun getItemType (item: Item) = Unit

		@Suppress("ConvertSecondaryConstructorToPrimary")
		@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)

	}


}