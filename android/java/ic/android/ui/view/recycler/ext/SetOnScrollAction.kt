package ic.android.ui.view.recyclerview.ext


import androidx.recyclerview.widget.RecyclerView


fun RecyclerView.setOnScrollAction (

	toCallAtOnce : Boolean = false,

	onScrollAction : (scrollPositionPx: Int, scrollShiftPx: Int) -> Unit

) {

	addOnScrollListener(
		object : RecyclerView.OnScrollListener() {
			override fun onScrolled (recyclerView: RecyclerView, dx: Int, dy: Int) {
				onScrollAction(computeVerticalScrollOffset(), dy)
			}
		}
	)

	if (toCallAtOnce) {
		onScrollAction(computeVerticalScrollOffset(), 0)
	}

}