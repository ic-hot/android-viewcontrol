package ic.android.ui.view.container


import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewControllerWithTransAndEnv


internal typealias UntypedViewController
	= GenerativeStatefulViewControllerWithEnv<*, *>
;

internal typealias TypedStatefulViewController<State, Env>
	= GenerativeStatefulViewControllerWithEnv<State, Env>
;

internal typealias TypedSetStateViewController<State, Transition, Env>
	= GenerativeSetStateViewControllerWithTransAndEnv<State, Transition, Env>
;