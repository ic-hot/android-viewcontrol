@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.container


import ic.ifaces.cancelable.Cancelable
import ic.ifaces.pausable.Pausable
import ic.struct.map.editable.EditableMap

import android.content.Context
import android.util.AttributeSet
import android.view.View

import ic.android.ui.view.layout.frame.FrameLayout
import ic.android.ui.view.trans.AndroidViewTransition
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.ui.view.sizePx


class ContainerView : FrameLayout {


	init {
		clipChildren = false
		clipToPadding = false
	}


	private val viewEnvironment = AndroidViewScope(
		context = context,
		parentView = this
	)


	private var contentViewController : UntypedViewController? = null

	private var contentView : View? = null


	private class ClosingTransition (
		val view : View,
		val transitionTask : Cancelable
	)


	private val closingTransitions = EditableMap<UntypedViewController, ClosingTransition>()


	private fun implementClose (externalTransition: AndroidViewTransition) {
		val view = contentView!!
		val viewController = contentViewController!!
		val transitionTask = externalTransition.implementCloseTransition(
			view = view,
			parentSizePx = sizePx,
			onComplete = {
				closingTransitions[viewController] = null
				if (viewController is Pausable)	viewController.pause()
				viewController.close()
				removeView(view)
			}
		)
		if (transitionTask != null) {
			closingTransitions[viewController] = ClosingTransition(
				view = view,
				transitionTask = transitionTask
			)
		}
	}


	private fun <State, Env> implementSetContentView (
		viewController : TypedStatefulViewController<State, Env>,
		state : State,
		environment : Env,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None
	) {

		if (this.contentViewController != null) {
			implementClose(externalTransition = externalTransition)
		}

		this.contentViewController = viewController
		val closingTransition = closingTransitions[viewController]
		if (closingTransition == null) {
			val view = viewController.openWithState(state, environment)
			contentView = view
			addView(view)
		} else {
			contentView = closingTransition.view
			closingTransitions[viewController] = null
			closingTransition.transitionTask.cancel()
		}
		externalTransition.implementOpenTransition(contentView!!, parentSizePx = sizePx)
		if (viewController is Pausable) viewController.resume()

	}


	fun <State, Env> setContentView (
		viewController : TypedStatefulViewController<State, Env>?,
		state : State,
		environment : Env,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None
	) {

		when {
			viewController == null -> {
				close(externalTransition = externalTransition)
			}
			viewController is TypedSetStateViewController<*, *, *> -> {
				@Suppress("UNCHECKED_CAST")
				val castedViewController = (
					viewController as TypedSetStateViewController<State, Unit, Env>?
				)
				setContentView(
					viewController = castedViewController,
					state = state,
					environment = environment,
					externalTransition = externalTransition
				)
			}
			else -> {
				implementSetContentView(
					viewController = viewController,
					state = state,
					environment = environment,
					externalTransition = externalTransition
				)
			}
		}

	}

	fun <State> setContentView (
		viewController : TypedStatefulViewController<State, AndroidViewScope>,
		state : State,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setContentView(
			viewController = viewController,
			state = state,
			environment = viewEnvironment,
			externalTransition = externalTransition
		)
	}


	fun setContentView (
		viewController : TypedStatefulViewController<Unit, AndroidViewScope>?,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setContentView(
			viewController = viewController,
			state = Unit,
			environment = viewEnvironment,
			externalTransition = externalTransition
		)
	}


	fun <State, InternalTransition, Environment> setContentView (
		viewController : TypedSetStateViewController<State, InternalTransition, Environment>?,
		state : State,
		internalTransition : InternalTransition,
		environment : Environment,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		when {
			viewController == null -> {
				close(externalTransition = externalTransition)
			}
			viewController === this.contentViewController -> {
				viewController.setState(state, internalTransition)
			}
			else -> {
				implementSetContentView(
					viewController = viewController,
					state = state,
					environment = environment,
					externalTransition = externalTransition
				)
			}
		}
	}


	@JvmName("setSetStateContentView")
	inline fun <State, Environment> setContentView (
		viewController : TypedSetStateViewController<State, Unit, Environment>?,
		state : State,
		environment : Environment,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setContentView(
			viewController = viewController,
			state = state,
			internalTransition = Unit,
			environment = environment,
			externalTransition = externalTransition
		)
	}

	@JvmName("setSetStateContentView")
	fun <State> setContentView (
		viewController : TypedSetStateViewController<State, Unit, AndroidViewScope>,
		state : State,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setContentView(
			viewController = viewController,
			state = state,
			internalTransition = Unit,
			environment = viewEnvironment,
			externalTransition = externalTransition
		)
	}


	@JvmName("setStatelessContentView")
	inline fun <InternalTransition, Environment> setContentView (
		viewController : TypedSetStateViewController<Unit, InternalTransition, Environment>,
		internalTransition : InternalTransition,
		environment : Environment,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setContentView(
			viewController = viewController,
			state = Unit,
			internalTransition = internalTransition,
			environment = environment,
			externalTransition = externalTransition
		)
	}


	fun close (externalTransition: AndroidViewTransition = AndroidViewTransition.None) {
		if (this.contentViewController != null) {
			implementClose(externalTransition = externalTransition)
		}
		this.contentViewController = null
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}

