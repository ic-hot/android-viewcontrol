@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.container.ext


import ic.android.ui.view.container.ContainerView
import ic.android.ui.view.control.gen.alias.GenerativeSetStateViewControllerWithTransAndEnv
import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.android.ui.view.scope.AndroidViewScope

import ic.gui.trans.SimpleTransition
import ic.gui.trans.ext.asAndroidViewTransition


inline fun <State, Environment> ContainerView.setContentView (
	viewController : GenerativeSetStateViewControllerWithTransAndEnv<State, Unit, Environment>,
	state : State,
	environment : Environment,
	externalTransition : SimpleTransition,
) {

	setContentView(
		viewController = viewController,
		state = state,
		environment = environment,
		externalTransition = externalTransition.asAndroidViewTransition
	)

}


inline fun <State> ContainerView.setContentView (
	viewController : GenerativeSetStateViewControllerWithTransAndEnv<State, Unit, AndroidViewScope>,
	state : State,
	externalTransition : SimpleTransition,
) {

	setContentView(
		viewController = viewController,
		state = state,
		externalTransition = externalTransition.asAndroidViewTransition
	)

}


inline fun ContainerView.setContentView (
	viewController : GenerativeStatefulViewControllerWithEnv<Unit, AndroidViewScope>?,
	externalTransition : SimpleTransition,
) {

	setContentView(
		viewController = viewController,
		externalTransition = externalTransition.asAndroidViewTransition
	)

}