package ic.android.ui.view.container.ext


import ic.android.ui.view.container.ContainerView
import ic.android.ui.view.control.gen.BaseGenerativeViewController
import ic.base.primitives.int32.Int32


fun ContainerView.setContentView (layoutResId: Int32) {

	setContentView(
		object : BaseGenerativeViewController() {
			override fun initSubject() = inflateView(layoutResId)
		}
	)

}