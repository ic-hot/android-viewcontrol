package ic.android.ui.view.container.ext


import ic.android.ui.view.container.ContainerView
import ic.android.ui.view.control.fromguivc.ViewControllerFromGuiViewController

import ic.gui.control.ViewController


fun ContainerView.setContentView (viewController: ViewController) {

	setContentView(
		ViewControllerFromGuiViewController(viewController)
	)

}