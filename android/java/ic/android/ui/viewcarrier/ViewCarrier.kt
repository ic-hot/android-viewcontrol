@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.pattern.carrier.Carrier

import android.app.Activity
import android.view.View
import android.view.ViewGroup


@Deprecated("Use ViewController")
abstract class ViewCarrier<State: Any, TransitionType: Any>
	: Carrier<View, State, ViewCarrier.Environment, TransitionType>()
{

	class Environment (
		val activity	: Activity,
		val parentView	: ViewGroup?
	)

	protected open val activity get() = environment.activity

	abstract class UnitTransition<State: Any> : ViewCarrier<State, Unit>() {
	}

}