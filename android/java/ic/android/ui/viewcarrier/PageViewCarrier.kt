package ic.android.ui.viewcarrier


import ic.base.primitives.float32.Float32


@Deprecated("Use ItemController")
interface PageViewCarrier {

	fun onPageScroll (scrollPhase : Float32)

}