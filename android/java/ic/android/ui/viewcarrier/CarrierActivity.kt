@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.android.ui.activity.decor.DecoratedActivity
import ic.pattern.carrier.GenerativeCarrier

import android.os.Bundle
import android.view.View


@Deprecated("Use ViewControlActivity")
abstract class CarrierActivity<State: Any> : DecoratedActivity() {

	abstract fun initState() : State

	protected open fun implementParseState (bundle: Bundle) : State = initState()
	protected open fun implementSerializeState (bundle: Bundle, state: State) {}

	private var viewCarrier : GenerativeCarrier<View, State, ViewCarrier.Environment, *>? = null

	protected abstract fun initViewCarrier() : GenerativeCarrier<View, State, ViewCarrier.Environment, *>

	override fun onCreate (stateBundle: Bundle?) {
		super.onCreate(stateBundle)
		viewCarrier = initViewCarrier()
		val state = if (stateBundle == null) {
			initState()
		} else {
			implementParseState(stateBundle)
		}
		val contentView = viewCarrier!!.open(ViewCarrier.Environment(this, null), state)
		setContentView(contentView)
	}

	override fun onResume() {
		super.onResume()
		viewCarrier!!.resume()
	}

	override fun onPause() {
		super.onPause()
		viewCarrier!!.pause()
	}

	override fun onDestroy() {
		super.onDestroy()
		viewCarrier!!.close()
		viewCarrier = null
	}

	override fun saveStateToBundle (stateBundle: Bundle) {
		val viewCarrier = viewCarrier
		if (viewCarrier != null && viewCarrier.isStateApplied) {
			implementSerializeState(stateBundle, viewCarrier.state)
		}
	}

	abstract class UnitState : CarrierActivity<Unit>() {
		final override fun initState() = Unit
	}

}