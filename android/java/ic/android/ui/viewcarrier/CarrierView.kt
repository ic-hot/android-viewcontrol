@file:Suppress("LeakingThis", "DEPRECATION")


package ic.android.ui.viewcarrier


import ic.ifaces.cancelable.Cancelable
import ic.pattern.carrier.Carrier
import ic.pattern.carrier.GenerativeCarrier

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import ic.android.ui.view.trans.AndroidViewTransition

import ic.android.ui.view.layout.frame.FrameLayout
import ic.android.ui.view.sizePx


@Deprecated("Use ContainerView")
open class CarrierView : FrameLayout {


	init {
		clipChildren = false
		clipToPadding = false
	}


	private val environmentData = ViewCarrier.Environment(context as Activity, this)


	private var viewCarrier : GenerativeCarrier<View, *, ViewCarrier.Environment, *>? = null


	private var contentView : View? = null


	private class ClosingTransition (
		val view : View,
		val transitionJob : Cancelable
	)

	private val closingTransitions = mutableMapOf<Carrier<View, *, ViewCarrier.Environment, *>, ClosingTransition>()


	private fun implementOpen (transition: AndroidViewTransition) {
		val contentViewCarrier = viewCarrier!!
		val closingTransition = closingTransitions[contentViewCarrier]
		if (closingTransition == null) {
			val view = contentViewCarrier.openIfNeeded(environmentData)
			contentView = view
			addView(view)
		} else {
			contentView = closingTransition.view
			closingTransitions.remove(contentViewCarrier)
			closingTransition.transitionJob.cancel()
		}
		transition.implementOpenTransition(contentView!!, parentSizePx = sizePx)
	}

	private fun implementClose (toCloseViewCarrier: Boolean, viewTransition: AndroidViewTransition) {
		val view = contentView!!
		val viewCarrier = viewCarrier!!
		val transitionJob = viewTransition.implementCloseTransition(
			view = view,
			parentSizePx = sizePx,
			onComplete = {
				if (closingTransitions.contains(viewCarrier)) {
					closingTransitions.remove(viewCarrier)
				}
				if (toCloseViewCarrier) viewCarrier.close()
				removeView(view)
			}
		)
		if (transitionJob != null) {
			closingTransitions[viewCarrier] = ClosingTransition(
				view = view,
				transitionJob = transitionJob
			)
		}
	}


	fun <State: Any, CarrierTransition: Any> setViewCarrier (
		viewCarrier : GenerativeCarrier<View, State, ViewCarrier.Environment, CarrierTransition>,
		state : State,
		toCloseCurrentViewCarrier : Boolean = true,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
		internalTransition : CarrierTransition?
	) {
		if (viewCarrier === this.viewCarrier) {
			viewCarrier.setState(state, internalTransition)
		} else {
			if (this.viewCarrier != null) {
				implementClose(toCloseViewCarrier = toCloseCurrentViewCarrier, viewTransition = externalTransition)
			}
			this.viewCarrier = viewCarrier
			implementOpen(externalTransition)
			viewCarrier.initState(state)
			viewCarrier.resumeIfNeeded()
		}
	}

	fun <State: Any> setViewCarrier (
		viewCarrier : GenerativeCarrier<View, State, ViewCarrier.Environment, Unit>,
		state : State,
		toCloseCurrentViewCarrier : Boolean = true,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
	) {
		setViewCarrier(
			viewCarrier = viewCarrier,
			toCloseCurrentViewCarrier = toCloseCurrentViewCarrier,
			state = state,
			externalTransition = externalTransition,
			internalTransition = Unit
		)
	}

	fun <CarrierTransition: Any> setViewCarrier (
		viewCarrier : GenerativeCarrier<View, Unit, ViewCarrier.Environment, CarrierTransition>,
		toCloseCurrentViewCarrier : Boolean = true,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None,
		internalTransition : CarrierTransition?
	) {
		setViewCarrier(
			viewCarrier = viewCarrier,
			toCloseCurrentViewCarrier = toCloseCurrentViewCarrier,
			state = Unit,
			externalTransition = externalTransition,
			internalTransition = internalTransition
		)
	}

	fun setViewCarrier (
		viewCarrier : GenerativeCarrier<View, Unit, ViewCarrier.Environment, Unit>,
		toCloseCurrentViewCarrier : Boolean = true,
		externalTransition : AndroidViewTransition = AndroidViewTransition.None
	) {
		setViewCarrier(
			viewCarrier = viewCarrier,
			toCloseCurrentViewCarrier = toCloseCurrentViewCarrier,
			state = Unit,
			externalTransition = externalTransition,
			internalTransition = Unit
		)
	}


	fun close (toCloseViewCarrier : Boolean = true, viewTransition: AndroidViewTransition = AndroidViewTransition.None) {
		if (this.viewCarrier != null) {
			implementClose(toCloseViewCarrier = toCloseViewCarrier, viewTransition = viewTransition)
		}
		this.viewCarrier = null
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}