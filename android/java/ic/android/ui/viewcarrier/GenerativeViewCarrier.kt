@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.pattern.carrier.GenerativeCarrier

import android.view.View


@Deprecated("Use GenerativeViewController")
abstract class GenerativeViewCarrier<State: Any, TransitionType: Any>

	: GenerativeCarrier<View, State, ViewCarrier.Environment, TransitionType>()

{

	protected fun inflateView (resId: Int) : View {
		val environmentData = environment
		return environmentData.activity.layoutInflater.inflate(resId, environmentData.parentView, false)
	}

	open val activity get() = environment.activity

	abstract class UnitTransition<State: Any> : GenerativeViewCarrier<State, Unit>() {

		override var state: State
			get() = super.state
			set(value) {
				setState(
					value,
					transition = if (isStateApplied) Unit else null
				)
			}
		;

	}

	abstract class UnitState : UnitTransition<Unit>() {
		override fun onUpdateState (subject: View, state: Unit, transition: Unit?) {}
	}

}