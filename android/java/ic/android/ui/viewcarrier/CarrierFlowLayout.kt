@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.base.assert.assert
import ic.pattern.carrier.Carrier
import ic.struct.list.List

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View

import ic.android.ui.view.layout.flow.FlowLayout
import ic.base.primitives.int64.asInt32
import ic.struct.list.ext.foreach.breakableForEach
import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.ext.foreach.breakableForEachIndexed
import ic.struct.list.ext.reduce.find.atLeastOne


@Suppress("UNCHECKED_CAST")


@Deprecated("Use RecyclingFlowLayout")
abstract class CarrierFlowLayout<Item: Any> : FlowLayout {


	private val environmentData = ViewCarrier.Environment(context as Activity, this)


	protected abstract fun generateItemViewCarrier() : GenerativeCarrier<View, Item, ViewCarrier.Environment, *>


	// Entries:

	private class Entry (
		var carrier : GenerativeCarrier<View, *, ViewCarrier.Environment, *>,
		var item : Any
	)

	private val entries = mutableListOf<Entry>()


	private var items : List<*>? = null

	fun setItems (items: List<Item>) {
		this.items = items
		entries.forEach { it.carrier.close() }
		entries.clear()
		removeAllViews()
		items.breakableForEach { item ->
			val itemViewCarrier = generateItemViewCarrier()
			val itemView = itemViewCarrier.open(environmentData)
			entries.add(Entry(itemViewCarrier, item))
			addView(itemView)
			itemViewCarrier.initState(item)
		}
	}


	fun updateItems (items: List<Item>) {
		this.items = items
		items.breakableForEachIndexed { index, item ->
			val itemViewCarrier : Carrier<View, Item, ViewCarrier.Environment, *>
			val itemView : View
			val entry = entries.find { it.item == item }
			if (entry == null) {
				itemViewCarrier = generateItemViewCarrier()
				itemView = itemViewCarrier.open(environmentData)
				val insertTo = if (index == 0L) 0 else {
					val previousItem = items[index - 1]
					entries.indexOfFirst { it.item == previousItem } + 1
				}
				entries.add(insertTo, Entry(itemViewCarrier, item))
				addView(itemView, insertTo)
				itemViewCarrier.initState(item)
			} else {
				itemViewCarrier = entry.carrier as Carrier<View, Item, ViewCarrier.Environment, *>
				itemViewCarrier.initState(item)
				entry.item = item
			}
		}
		var i = 0; while (i < entries.size) { val entry = entries[i]
			if (items.atLeastOne { i == entry.item }) {
				i++
			} else {
				entries.removeAt(i)
				removeViewAt(i)
			}
		}
	}


	fun updateAllItems (items: List<Item>) {
		assert { items.length.asInt32 == entries.size }
		items.breakableForEachIndexed { index, item ->
			val itemViewCarrier = entries[index.asInt32].carrier as Carrier<View, Item, ViewCarrier.Environment, *>
			itemViewCarrier.initState(item)
			entries[index.asInt32].item = item
		}
	}


	fun close() {
		entries.forEach { it.carrier.close() }
		entries.clear()
		removeAllViews()
		items = null
	}


	@Suppress("ConvertSecondaryConstructorToPrimary") @JvmOverloads constructor (context: Context, attrs: AttributeSet? = null) : super(context, attrs)


}