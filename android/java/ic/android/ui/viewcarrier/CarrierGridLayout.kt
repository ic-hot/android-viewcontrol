@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.struct.list.List

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.Space
import ic.android.ui.view.ext.activity

import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.ext.foreach.breakableForEachIndexed
import ic.struct.list.ext.length.isEmpty


@Suppress("UNCHECKED_CAST")

@Deprecated("Use RecyclingGridLayout")
abstract class CarrierGridLayout<Item: Any> : LinearLayout {


	init {
		orientation = VERTICAL
		clipChildren = false
		clipToPadding = false
	}


	protected abstract fun generateItemViewCarrier() : GenerativeCarrier<View, Item, ViewCarrier.Environment, *>

	protected abstract val columnsCount : Int


	// Entries:

	private class Entry (
		var carrier : GenerativeCarrier<View, *, ViewCarrier.Environment, *>,
		var item : Any
	)

	private val entries = mutableListOf<Entry>()


	private var items : List<*>? = null

	fun setItems (items: List<Item>) {
		this.items = items
		entries.forEach { it.carrier.close() }
		entries.clear()
		removeAllViews()
		if (items.isEmpty) return
		val columnsCount = columnsCount
		var row : LinearLayout? = null
		items.breakableForEachIndexed { index, item ->
			if (index % columnsCount == 0L) {
				if (row != null) {
					addView(row, LayoutParams(MATCH_PARENT, WRAP_CONTENT))
				}
				row = LinearLayout(context).apply {
					clipChildren = false
					clipToPadding = false
					orientation = HORIZONTAL
				}
			}
			val itemViewCarrier = generateItemViewCarrier()
			val itemView = itemViewCarrier.open(
				ViewCarrier.Environment(
					activity = activity,
					parentView = row!!
				)
			)
			entries.add(Entry(itemViewCarrier, item))
			row!!.addView(itemView)
			itemViewCarrier.initState(item)
		}
		if (row!!.childCount > 0) {
			while (row!!.childCount < columnsCount) {
				row!!.addView(Space(context), row!!.getChildAt(0).layoutParams)
			}
			addView(row, LayoutParams(MATCH_PARENT, WRAP_CONTENT))
		}
	}


	fun close() {
		entries.forEach { it.carrier.close() }
		entries.clear()
		removeAllViews()
		items = null
	}


	@Suppress("ConvertSecondaryConstructorToPrimary") @JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)


}