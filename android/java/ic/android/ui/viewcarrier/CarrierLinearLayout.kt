@file:Suppress("FunctionName", "DEPRECATION")


package ic.android.ui.viewcarrier


import ic.ifaces.cancelable.Cancelable
import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEachIndexed

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View

import ic.android.ui.view.layout.linear.LinearLayout
import ic.android.util.handler.postForEachIndexed
import ic.base.primitives.int64.asInt32


@Deprecated("Use RecyclerLinearLayout")
abstract class CarrierLinearLayout<Item: Any> : LinearLayout {


	private val environmentData = ViewCarrier.Environment(context as Activity, this)


	protected abstract inner class ItemController<ViewCarrierType: GenerativeCarrier<View, Item, ViewCarrier.Environment, *>> {
		abstract val viewCarrier : ViewCarrierType
		abstract fun onIndexChanged (index: Int)
	}

	protected inline fun <ViewCarrierType: GenerativeCarrier<View, Item, ViewCarrier.Environment, *>> ItemController (
		viewCarrier : ViewCarrierType,
		crossinline onIndexChanged : ItemController<ViewCarrierType>.(index: Int) -> Unit = {}
	) : ItemController<*> {
		return object : ItemController<ViewCarrierType>() {
			override val viewCarrier get() = viewCarrier
			override fun onIndexChanged(index: Int) = onIndexChanged.invoke(this, index)
		}
	}

	protected abstract fun generateItemController() : ItemController<*>


	// Entries:

	private inner class Entry (
		var itemController : ItemController<*>
	)

	private val entries = mutableListOf<Entry>()

	fun setItems (items: List<Item>) {

		entries.forEach { it.itemController.viewCarrier.close() }
		entries.clear()
		removeAllViews()
		items.breakableForEachIndexed { index, item ->

			@Suppress("UNCHECKED_CAST")
			val itemController = generateItemController()
				as ItemController<GenerativeCarrier<View, Item, ViewCarrier.Environment, *>>
			;
			val itemView = itemController.viewCarrier.open(environmentData)
			entries.add(Entry(itemController))
			addView(itemView)
			itemController.viewCarrier.initState(item)
			itemController.onIndexChanged(index.asInt32)

		}

	}


	private var postSetItemsJob : Cancelable? = null

	fun postSetItems (items: List<Item>) : Cancelable? {

		postSetItemsJob?.cancel()

		entries.forEach { it.itemController.viewCarrier.close() }
		entries.clear()
		removeAllViews()

		postSetItemsJob = items.postForEachIndexed(batchSize = 16) { index, item ->

			@Suppress("UNCHECKED_CAST")
			val itemController = generateItemController()
				as ItemController<GenerativeCarrier<View, Item, ViewCarrier.Environment, *>>
			;
			val itemView = itemController.viewCarrier.open(environmentData)
			entries.add(Entry(itemController))
			addView(itemView)
			itemController.viewCarrier.initState(item)
			itemController.onIndexChanged(index)

		}

		return postSetItemsJob

	}


	fun close() {
		entries.forEach { it.itemController.viewCarrier.close() }
		entries.clear()
		removeAllViews()
		postSetItemsJob = null
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}