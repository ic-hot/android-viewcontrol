@file:Suppress("DEPRECATION")

package ic.android.ui.viewcarrier


import ic.struct.map.editable.EditableMap

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import ic.base.primitives.int64.asInt32

import ic.pattern.carrier.GenerativeCarrier
import ic.struct.list.List
import ic.struct.list.ext.get


@Deprecated("")
abstract class CarrierListView<Item: Any> : ListView {


	private val environmentData = ViewCarrier.Environment(context as Activity, this)


	private var items : List<Item>? = null


	protected abstract fun generateItemViewCarrier() : GenerativeCarrier<View, Item, ViewCarrier.Environment, *>


	private val viewCarriers = EditableMap<View, GenerativeCarrier<View, Item, ViewCarrier.Environment, *>>()


	fun setItems (items : List<Item>, toAnimate: Boolean) {
		this.items = items
		if (toAnimate) {
			(adapter as BaseAdapter?)?.notifyDataSetChanged()
		} else {
			adapter = object : BaseAdapter() {
				override fun getCount() = this@CarrierListView.items!!.length.asInt32
				override fun getItemId (position: Int) = position.toLong()
				override fun getItem (position: Int) = this@CarrierListView.items!![position]
				override fun getView (position: Int, convertView: View?, parent: ViewGroup?) : View {
					val item = this@CarrierListView.items!![position]
					if (convertView == null) {
						val viewCarrier = generateItemViewCarrier()
						val view = viewCarrier.open(environmentData)
						viewCarrier.initState(item)
						viewCarriers[view] = viewCarrier
						return view
					} else {
						val viewCarrier = viewCarriers[convertView]!!
						viewCarrier.initState(item)
						return convertView
					}
				}
			}
		}
	}

	fun setItems (items : List<Item>) = setItems(items, toAnimate = this.items != null)


	@Suppress("ConvertSecondaryConstructorToPrimary") @JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)


}