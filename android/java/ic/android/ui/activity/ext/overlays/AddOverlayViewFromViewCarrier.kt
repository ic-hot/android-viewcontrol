@file:Suppress("DEPRECATION")


package ic.android.ui.activity.ext.overlays


import ic.ifaces.cancelable.Cancelable

import android.app.Activity

import ic.android.ui.viewcarrier.GenerativeViewCarrier
import ic.android.ui.viewcarrier.ViewCarrier


fun <State: Any> Activity.addOverlayView (viewCarrier: GenerativeViewCarrier<State, *>, state: State) : Cancelable {
	val view = viewCarrier.open(ViewCarrier.Environment(this, findViewById(android.R.id.content)))
	addOverlayView(view)
	viewCarrier.initState(state)
	viewCarrier.resume()
	return object : Cancelable {
		override fun cancel() {
			viewCarrier.close()
			removeOverlayView(view)
		}
	}
}