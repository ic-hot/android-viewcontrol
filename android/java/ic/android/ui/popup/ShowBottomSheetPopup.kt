package ic.android.ui.popup


import android.app.Activity

import ic.android.ui.activity.ext.activity
import ic.android.ui.activity.ext.popup.PopupController
import ic.android.ui.activity.ext.popup.showPopup
import ic.android.ui.viewcarrier.ViewCarrier
import ic.base.primitives.int32.Int32
import ic.graphics.color.ColorArgb
import ic.gui.align.Bottom
import ic.gui.dim.px.ext.inDp
import ic.ifaces.cancelable.Cancelable


@Deprecated("")
fun <State: Any> Activity.showBottomSheetPopup (

	popupCarrier : PopupCarrier<State>,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int = ColorArgb(0x00000000),
	backgroundBlurRadiusPx : Int32 = 0

) : Cancelable {

	val popup = showPopup(
		popupController = PopupController(
			createView = {
				val popupViewEnv = this
				popupCarrier.open(
					object : PopupCarrier.Environment() {
						override val viewCarrierEnvironment = ViewCarrier.Environment(activity, parentView)
						override fun closePopup() = popupViewEnv.closePopup()
					}
				)
			},
			destroyView = {
				popupCarrier.close()
			},
			onClose = { _, _ -> },
			onOpen = {}
		),
		verticalAlign = Bottom,
		isCloseable = isCloseable,
		backgroundTintColor = backgroundTintColorArgb,
		backgroundBlurRadius = backgroundBlurRadiusPx.inDp
	)

	return Cancelable(
		cancel = { popup.close() }
	)

}