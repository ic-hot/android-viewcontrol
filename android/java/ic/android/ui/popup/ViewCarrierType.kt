@file:Suppress("DEPRECATION")


package ic.android.ui.popup

import android.view.View
import ic.android.ui.viewcarrier.ViewCarrier
import ic.pattern.carrier.GenerativeCarrier


internal typealias ViewCarrierType<State> = GenerativeCarrier<View, State, ViewCarrier.Environment, *>