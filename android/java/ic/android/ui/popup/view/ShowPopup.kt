@file:Suppress("DEPRECATION")


package ic.android.ui.popup.view


import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.MotionEvent
import android.view.View
import android.view.View.MeasureSpec.UNSPECIFIED
import android.view.View.MeasureSpec.makeMeasureSpec
import android.widget.FrameLayout
import android.widget.PopupWindow

import ic.android.ui.popup.PopupCarrier
import ic.android.ui.view.ext.activity
import ic.android.ui.viewcarrier.ViewCarrier
import ic.android.util.handler.postDelayed


fun <State: Any> View.showPopup (

	popupCarrier : PopupCarrier<State>,

	toOverlapAnchor : Boolean

) {

	lateinit var popupWindow : PopupWindow

	val containerView = FrameLayout(context).apply {
		clipToPadding = false
		clipChildren = false
	}

	val contentView = popupCarrier.open(
		object : PopupCarrier.Environment() {
			@SuppressLint("StaticFieldLeak")
			override val viewCarrierEnvironment = ViewCarrier.Environment(activity, containerView)
			override fun closePopup() {
				popupWindow.dismiss()
			}
		}
	)

	containerView.addView(contentView)

	containerView.measure(makeMeasureSpec(0, UNSPECIFIED), makeMeasureSpec(0, UNSPECIFIED))

	popupWindow = PopupWindow(containerView, containerView.measuredWidth, containerView.measuredHeight)
	popupWindow.setBackgroundDrawable(ColorDrawable())
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
		popupWindow.isTouchModal = false
	}
	popupWindow.isFocusable = false
	popupWindow.isOutsideTouchable = true
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
		popupWindow.overlapAnchor = toOverlapAnchor
	}
	popupWindow.showAsDropDown(this)
	popupWindow.setTouchInterceptor { view, event ->
		if (event.action == MotionEvent.ACTION_OUTSIDE) {
			popupWindow.dismiss()
			view.performClick()
			true
		} else {
			false
		}
	}
	popupWindow.setOnDismissListener {
		popupCarrier.onClosePopup()
		postDelayed(1024) {
			popupCarrier.close()
		}
	}

}