@file:Suppress("FunctionName", "PROTECTED_CALL_FROM_PUBLIC_INLINE", "TYPEALIAS_EXPANSION_DEPRECATION")


package ic.android.ui.popup



inline fun <State: Any> PopupCarrier (

	crossinline initViewCarrier : PopupCarrier<State>.() -> ViewCarrierType<State>,

	initialState : State,

	crossinline onClosePopup : () -> Unit = {}

) : PopupCarrier<State> {

	return object : PopupCarrier<State>() {

		override fun initViewCarrier() : ViewCarrierType<State> = initViewCarrier.invoke(this)

		override val initialViewState get() = initialState

		override fun onClosePopup() = onClosePopup()

	}

}


inline fun PopupCarrier (

	crossinline initViewCarrier : PopupCarrier<Unit>.() -> ViewCarrierType<Unit>,

	crossinline onClosePopup : () -> Unit = {}

) : PopupCarrier<Unit> {

	return PopupCarrier(
		initViewCarrier = initViewCarrier,
		initialState = Unit,
		onClosePopup = onClosePopup
	)
}