@file:Suppress("NOTHING_TO_INLINE", "DEPRECATION", "TYPEALIAS_EXPANSION_DEPRECATION")


package ic.android.ui.popup


import ic.pattern.carrier.GenerativeCarrier

import android.view.View

import ic.android.ui.viewcarrier.ViewCarrier


abstract class PopupCarrier<ViewState: Any>

	: GenerativeCarrier.UnitState<View, PopupCarrier.Environment>()

{


	abstract class Environment {

		abstract val viewCarrierEnvironment : ViewCarrier.Environment

		abstract fun closePopup()

	}


	protected abstract fun initViewCarrier() : ViewCarrierType<ViewState>

	protected abstract val initialViewState : ViewState


	private var viewCarrier : ViewCarrierType<ViewState>? = null


	override fun initSubject() : View {

		viewCarrier = initViewCarrier()

		return viewCarrier!!.open(environment.viewCarrierEnvironment, initialViewState)

	}


	override fun onResume() {

		viewCarrier!!.resume()

	}


	override fun onPause() {

		viewCarrier!!.pause()

	}


	override fun onClose() {

		viewCarrier!!.close()

		viewCarrier = null

	}


	open fun onClosePopup() {}


	fun closePopup() {
		if (!isOpen) return
		environment.closePopup()
	}


}