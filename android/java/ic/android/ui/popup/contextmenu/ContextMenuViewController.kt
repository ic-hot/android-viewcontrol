package ic.android.ui.popup.contextmenu


import ic.base.R
import ic.design.control.list.ListController
import ic.ifaces.stateful.ext.state
import ic.struct.list.List

import ic.gui.dim.dp.ext.dp

import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.LinearLayout.VERTICAL

import ic.android.ui.popup.AndroidPopupScope
import ic.android.ui.view.control.gen.BaseGenerativeViewControllerWithEnv
import ic.android.ui.view.control.list.openAsListController
import ic.android.ui.view.ext.elevationDp
import ic.android.ui.view.ext.setBackgroundRoundCorners
import ic.android.ui.view.layout.linear.LinearLayout
import ic.android.storage.res.getResColorArgb
import ic.android.util.units.dpToPx


abstract class ContextMenuViewController : BaseGenerativeViewControllerWithEnv<AndroidPopupScope>() {

	abstract val items : List<ContextMenuItem>

	private var itemsListController : ListController<ContextMenuItem>? = null

	override fun initSubject() = LinearLayout(context).apply {
		layoutParams = FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
			leftMargin = dpToPx(16)
			rightMargin = dpToPx(16)
			topMargin = dpToPx(16)
			bottomMargin = dpToPx(16)
		}
		orientation = VERTICAL
		setBackgroundRoundCorners(
			color = getResColorArgb(R.color.white),
			cornersRadius = 8.dp
		)
		elevationDp = 8.dp
		itemsListController = openAsListController(
			generateItemViewController = {
				ContextMenuItemViewController(
					closePopup = { environment.closePopup() }
				)
			}
		)
	}

	override fun onOpen() {
		itemsListController!!.state = items
	}

	override fun onClose() {
		itemsListController!!.close()
		itemsListController = null
	}

}