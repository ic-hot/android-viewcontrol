package ic.android.ui.popup.contextmenu


abstract class ContextMenuItem {

	abstract val text : String

	abstract fun onClick()

}