@file:Suppress("FunctionName")


package ic.android.ui.popup.contextmenu

import ic.android.storage.res.getResString


inline fun ContextMenuItem (

	text : String,

	crossinline onClick : () -> Unit

) : ContextMenuItem {

	return object : ContextMenuItem() {

		override val text get() = text

		override fun onClick() = onClick()

	}

}


inline fun ContextMenuItem (

	textResId : Int,

	crossinline onClick : () -> Unit

) : ContextMenuItem {

	return ContextMenuItem(
		text = getResString(textResId),
		onClick = onClick
	)

}