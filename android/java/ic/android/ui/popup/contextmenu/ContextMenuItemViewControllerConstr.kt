package ic.android.ui.popup.contextmenu


fun ContextMenuItemViewController (

	closePopup : () -> Unit

) : ContextMenuItemViewController {

	return object : ContextMenuItemViewController() {

		override fun closePopup() = closePopup()

	}

}