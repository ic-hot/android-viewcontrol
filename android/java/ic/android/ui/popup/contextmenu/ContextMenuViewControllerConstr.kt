@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.popup.contextmenu


import ic.struct.list.List


inline fun ContextMenuViewController (

	items : List<ContextMenuItem>

) : ContextMenuViewController {

	return object : ContextMenuViewController() {

		override val items get() = items

	}

}