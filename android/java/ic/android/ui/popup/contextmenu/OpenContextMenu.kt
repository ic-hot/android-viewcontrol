@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.popup.contextmenu


import ic.struct.list.List

import android.view.View

import ic.android.ui.popup.view.openPopup


fun View.openContextMenu (

	items : List<ContextMenuItem>

) {

	openPopup(

		viewController = ContextMenuViewController(
			items = items
		),

		toOverlapAnchor = false

	)

}


inline fun View.openContextMenu (

	vararg items : ContextMenuItem

) {

	openContextMenu(List(*items))

}