package ic.android.ui.popup.contextmenu


import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout

import ic.android.ui.view.control.gen.BaseGenerativeSetStateViewController
import ic.android.ui.view.ext.*
import ic.android.ui.view.text.TextView
import ic.android.ui.view.text.ext.textColorArgb
import ic.android.ui.view.text.ext.textSizePx
import ic.android.ui.view.text.ext.value
import ic.android.storage.res.getResColorArgb
import ic.android.util.units.dpToPx
import ic.android.util.units.spToPx

import ic.base.R


abstract class ContextMenuItemViewController : BaseGenerativeSetStateViewController<ContextMenuItem>() {


	protected abstract fun closePopup()


	override fun initSubject() = TextView(activity).apply {
		layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
		leftPaddingPx = dpToPx(16)
		rightPaddingPx = dpToPx(16)
		topPaddingPx = dpToPx(8)
		bottomPaddingPx = dpToPx(8)
		textColorArgb = getResColorArgb(R.color.darkGray)
		textSizePx = spToPx(14)
		isAllCaps = true
		isClickable = true
	}

	override fun onSetState (state: ContextMenuItem) {

		(subject as TextView).value = state.text

		subject.setOnClickAction {
			closePopup()
			state.onClick()
		}

	}


}