@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.popup.activity.bottom


import android.app.Activity
import ic.android.storage.res.getResColorArgb
import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.android.ui.view.scope.AndroidViewScope
import ic.android.util.units.dpToPx
import ic.base.R
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


inline fun Activity.openBottomPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<Unit, AndroidViewScope>,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = getResColorArgb(R.color.transparent),
	backgroundBlurRadiusDp : Float32 = Float32(0),

	noinline onClose : () -> Unit = {}

) = openBottomPopup(
	viewController = viewController,
	initEnvironment = { it },
	isCloseable = isCloseable,
	backgroundTintColorArgb = backgroundTintColorArgb,
	backgroundBlurRadiusPx = dpToPx(backgroundBlurRadiusDp),
	onClose = onClose
)