package ic.android.ui.popup.activity.bottom.impl


import android.view.View
import ic.android.ui.activity.ext.screenSizePx
import ic.android.ui.view.ext.activity
import ic.android.ui.view.ext.measuredHeightPx

import ic.gui.dim.px.Px


internal fun View.measureHeightPx() : Px {

	val screenSizePx = activity.screenSizePx

	try {
		measure(
			View.MeasureSpec.makeMeasureSpec(screenSizePx.x, View.MeasureSpec.EXACTLY),
			View.MeasureSpec.makeMeasureSpec(screenSizePx.y, View.MeasureSpec.AT_MOST)
		)
		return measuredHeightPx
	} catch (t: Throwable) {
		return screenSizePx.y
	}

}