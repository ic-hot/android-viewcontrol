@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.popup.activity.bottom


import ic.base.primitives.float32.Float32
import ic.ifaces.lifecycle.closeable.Closeable

import android.app.Activity

import ic.base.R
import ic.android.ui.popup.AndroidPopupScope
import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.android.storage.res.getResColorArgb
import ic.android.ui.activity.ext.popup.bottom.openBottomPopup
import ic.android.ui.view.scope.AndroidViewScope
import ic.base.primitives.int32.Int32
import ic.android.util.units.dpToPx
import ic.ifaces.pausable.Pausable


fun <State, Env: AndroidViewScope> Activity.openBottomPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<State, Env>,

	initEnvironment : (AndroidPopupScope) -> Env,

	state : State,

	isCloseable : Boolean,

	backgroundTintColor : Int32 = getResColorArgb(R.color.transparent),
	backgroundBlurRadius : Int32 = 0,

	onClose : () -> Unit = {}

) : Closeable {

	return openBottomPopup(
		createView = {
			val view = viewController.openWithState(state = state, environment = initEnvironment(this))
			(viewController as? Pausable)?.resume()
			view
		},
		destroyView = {
			(viewController as? Pausable)?.pause()
			viewController.close()
		},
		isCloseable = isCloseable,
		backgroundTintColor = backgroundTintColor,
		backgroundBlurRadius = backgroundBlurRadius,
		onClose = { onClose() }
	)

}


inline fun <Env: AndroidViewScope> Activity.openBottomPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<Unit, Env>,

	noinline initEnvironment : (AndroidPopupScope) -> Env,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = getResColorArgb(R.color.transparent),
	backgroundBlurRadiusPx : Int32 = 0,

	noinline onClose : () -> Unit = {}

) = openBottomPopup(
	viewController = viewController,
	initEnvironment = initEnvironment,
	state = Unit,
	isCloseable = isCloseable,
	backgroundTintColor = backgroundTintColorArgb,
	backgroundBlurRadius = backgroundBlurRadiusPx,
	onClose = onClose
)


inline fun <State> Activity.openBottomPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<State, AndroidPopupScope>,

	state : State,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = getResColorArgb(R.color.transparent),
	backgroundBlurRadiusDp : Float32 = Float32(0),

	noinline onClose : () -> Unit = {}

) = openBottomPopup(
	viewController = viewController,
	state = state,
	initEnvironment = { it },
	isCloseable = isCloseable,
	backgroundTintColor = backgroundTintColorArgb,
	backgroundBlurRadius = dpToPx(backgroundBlurRadiusDp),
	onClose = onClose
)


inline fun Activity.openBottomPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<Unit, AndroidPopupScope>,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = getResColorArgb(R.color.transparent),
	backgroundBlurRadiusDp : Float32 = Float32(0),

	noinline onClose : () -> Unit = {}

) = openBottomPopup(
	viewController = viewController,
	initEnvironment = { it },
	isCloseable = isCloseable,
	backgroundTintColorArgb = backgroundTintColorArgb,
	backgroundBlurRadiusPx = dpToPx(backgroundBlurRadiusDp),
	onClose = onClose
)