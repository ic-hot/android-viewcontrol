@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.popup.activity


import ic.ifaces.cancelable.Cancelable

import android.app.Activity

import ic.android.ui.activity.ext.popup.PopupController
import ic.android.ui.activity.ext.popup.showPopup
import ic.android.ui.popup.AndroidPopupScope
import ic.android.ui.view.control.gen.alias.GenerativeStatefulViewControllerWithEnv
import ic.base.primitives.int32.Int32
import ic.graphics.color.ColorArgb
import ic.gui.dim.px.ext.inDp


fun <State> Activity.openPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<State, in AndroidPopupScope>,

	state : State,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = ColorArgb(0x00000000),
	backgroundBlurRadiusPx : Int32 = 0,

	onClose : () -> Unit = {}

) : Cancelable {

	val popup = showPopup(
		popupController = PopupController(
			createView = {
				viewController.openWithState(state, this)
			},
			destroyView = {
				viewController.close()
			},
			onClose = { _, _ -> onClose() },
			onOpen = {}
		),
		isCloseable = isCloseable,
		backgroundTintColor = backgroundTintColorArgb,
		backgroundBlurRadius = backgroundBlurRadiusPx.inDp
	)

	return Cancelable(
		cancel = { popup.close() }
	)

}


inline fun Activity.openPopup (

	viewController : GenerativeStatefulViewControllerWithEnv<Unit, in AndroidPopupScope>,

	isCloseable : Boolean,

	backgroundTintColorArgb : Int32 = ColorArgb(0x00000000),
	backgroundBlurRadiusPx : Int32 = 0,

	noinline onClose : () -> Unit = {}

) = openPopup(
	viewController = viewController,
	state = Unit,
	isCloseable = isCloseable,
	backgroundTintColorArgb = backgroundTintColorArgb,
	backgroundBlurRadiusPx = backgroundBlurRadiusPx,
	onClose = onClose
)