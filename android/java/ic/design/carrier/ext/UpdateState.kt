@file:Suppress("NOTHING_TO_INLINE", "DEPRECATION")


package ic.pattern.carrier.ext


import ic.pattern.carrier.Carrier


inline fun

	<Subject: Any, State: Any, Environment: Any>

	Carrier<Subject, State, Environment, Unit>

	.updateState()

{

	updateState(transition = Unit)

}