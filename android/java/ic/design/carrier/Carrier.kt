@file:Suppress("DEPRECATION")

package ic.pattern.carrier


import ic.base.assert.assert


/**
 * Universal class to manage subject's lifecycle and state, whatever subject is.
 * Can be used under Activity, Fragment, Layout, various adapters, etc.
 */
@Deprecated("Use Controller")
abstract class Carrier<Subject: Any, State: Any, Environment: Any, Transition: Any> {


	// Environment data:

	internal var environmentField : Environment? = null
	protected val environment get() = environmentField!!


	// Subject:

	internal var subjectField : Subject? = null
	protected val subject get() = subjectField!!

	protected open fun onOpen (subject: Subject) {}

	fun open (subject: Subject, environment: Environment) {
		if (subjectField != null) close()
		this.environmentField = environment
		subjectField = subject
		onOpen(subject)
	}


	// State:

	private var stateField : State? = null

	open val state : State get() = snapshotState()

	protected open fun onUpdateState (subject: Subject, state: State, transition: Transition?) {}

	protected open fun afterStateChanged() {}

	fun initState (state: State) {
		stateField = state
		onUpdateState(subjectField!!, state, null)
		afterStateChanged()
	}

	fun initStateIfNeeded (state: State) {
		if (stateField == state) return
		initState(state)
	}

	fun updateState (state: State, transitionType: Transition) {
		if (subjectField == null) return
		onUpdateState(subjectField!!, state, transitionType)
		stateField = state
		afterStateChanged()
	}

	fun updateState (transition: Transition) {
		if (subjectField == null) return
		if (stateField == null) return
		onUpdateState(subjectField!!, state, transition)
		afterStateChanged()
	}

	fun setState (state: State, transition: Transition?) {
		if (subjectField == null) return
		if (transition == null) {
			initState(state)
		} else {
			updateState(state, transition)
		}
	}

	protected open fun snapshotState() : State = stateField!!


	val isOpen : Boolean get() = subjectField != null

	val isStateApplied : Boolean get() = stateField != null


	private var isResumed = false


	protected open fun onResume() {}

	fun resume() {
		assert { !isResumed }
		onResume()
		isResumed = true
	}

	fun resumeIfNeeded() {
		if (!isResumed) resume()
	}


	protected open fun onPause() {}

	fun pause() {
		assert { isResumed }
		isResumed = false
		onPause()
	}

	fun pauseIfNeeded() {
		if (isResumed) pause()
	}


	protected open fun onClose() {}

	fun close() {
		pauseIfNeeded()
		if (subjectField == null) return
		onClose()
		environmentField = null
		subjectField = null
		stateField = null
	}


	abstract class UnitTransition<Subject: Any, State: Any, Environment: Any>
		: Carrier<Subject, State, Environment, Unit>()
	{

		override var state: State
			get() = super.state
			set(value) {
				setState(
					value,
					transition = if (isStateApplied) Unit else null
				)
			}
		;

	}

	abstract class UnitState<Subject: Any, Environment: Any>
		: UnitTransition<Subject, Unit, Environment>()
	{
		override fun onUpdateState (subject: Subject, state: Unit, transition: Unit?) {}
	}


}