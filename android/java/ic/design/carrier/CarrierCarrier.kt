@file:Suppress("DEPRECATION")

package ic.pattern.carrier


@Deprecated("Use Controller")
abstract class CarrierCarrier<

	InternalSubject : Any,
	ExternalCarrier : GenerativeCarrier<out GenerativeCarrier<InternalSubject, *, EnvironmentData, *>, State, EnvironmentData, TransitionType>,
	State : Any,
	EnvironmentData : Any,
	TransitionType : Any

> : GenerativeCarrier<

	InternalSubject,
	State,
	EnvironmentData,
	TransitionType

>() {


	protected abstract fun initExternalCarrier() : ExternalCarrier

	private var externalCarrier : ExternalCarrier? = null

	final override fun initSubject() : InternalSubject {
		if (this.externalCarrier != null) throw IllegalStateException()
		val externalCarrier = initExternalCarrier()
		this.externalCarrier = externalCarrier
		return externalCarrier.open(environment).open(environment)
	}

	final override fun onResume() {
		externalCarrier!!.resume()
	}

	override fun onUpdateState (subject: InternalSubject, state: State, transition: TransitionType?) {
		if (transition == null) {
			externalCarrier!!.initState(state)
		} else {
			externalCarrier!!.updateState(state, transition)
		}
	}

	final override fun onPause() {
		externalCarrier!!.pause()
	}

	final override fun onClose() {
		externalCarrier?.close()
		externalCarrier = null
	}


	abstract class UnitTransition <

		InternalSubject : Any,
		ExternalCarrier : GenerativeCarrier<out GenerativeCarrier<InternalSubject, *, EnvironmentData, *>, State, EnvironmentData, Unit>,
		State : Any,
		EnvironmentData : Any

	> : CarrierCarrier<InternalSubject, ExternalCarrier, State, EnvironmentData, Unit>() {

	}


}