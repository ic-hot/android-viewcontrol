@file:Suppress("DEPRECATION")

package ic.pattern.carrier

import ic.util.log.logD


@Deprecated("Use Controller")
abstract class GenerativeCarrier <Subject: Any, State: Any, Environment: Any, Transition: Any>

	: Carrier<Subject, State, Environment, Transition>()

{


	protected abstract fun initSubject() : Subject


	private fun implementOpen (environment: Environment) : Subject {
		this.environmentField = environment
		val subject = initSubject()
		subjectField = subject
		onOpen(subject)
		return subject
	}

	fun open (environment: Environment) : Subject {
		if (subjectField != null) close()
		return implementOpen(environment)
	}

	fun openIfNeeded (environment: Environment) : Subject {
		if (subjectField != null) return subjectField!!
		return implementOpen(environment)
	}


	fun open (environment: Environment, initialState: State) : Subject {
		val subject = open(environment)
		initState(initialState)
		return subject
	}


	abstract class UnitTransition<Subject: Any, State: Any, Environment: Any>
		: GenerativeCarrier<Subject, State, Environment, Unit>()
	{

		override var state: State
			get() = super.state
			set(value) {
				setState(
					value,
					transition = if (isStateApplied) Unit else null
				)
			}
		;

	}

	abstract class UnitState<Subject: Any, Environment: Any> : GenerativeCarrier<Subject, Unit, Environment, Unit>() {
		override fun onUpdateState(subject: Subject, state: Unit, transition: Unit?) {}
	}


}