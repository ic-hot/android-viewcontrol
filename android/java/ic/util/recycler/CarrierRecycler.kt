@file:Suppress("SortModifiers", "PARAMETER_NAME_CHANGED_ON_OVERRIDE", "DEPRECATION")


package ic.util.recycler


import ic.ifaces.getter.getter1.Getter1
import ic.pattern.carrier.GenerativeCarrier


@Deprecated("")
abstract class CarrierRecycler <Key: Any, Value: Any, Environment: Any> : Getter1<Value, Key> {


	protected abstract val environment : Environment

	protected abstract fun generateValueCarrier() : GenerativeCarrier<Value, Key, Environment, *>

	protected abstract fun isInUse (key: Key) : Boolean


	private class Entry<Key: Any, Value: Any, Environment: Any> (
		val valueCarrier : GenerativeCarrier<Value, Key, Environment, *>,
		val value : Value
	)


	private val recycler = object : Recycler<Key, Entry<Key, Value, Environment>>() {

		override fun generateValue () : Entry<Key, Value, Environment> {
			val valueCarrier = generateValueCarrier()
			val value = valueCarrier.open(environment)
			return Entry(
				valueCarrier = valueCarrier,
				value = value
			)
		}

		override fun onSeize (key: Key, entry: Entry<Key, Value, Environment>) {
			entry.valueCarrier.initState(key)
			entry.valueCarrier.resume()
		}

		override fun onRelease (entry: Entry<Key, Value, Environment>) {
			entry.valueCarrier.pause()
		}

		override fun onClose (entry: Entry<Key, Value, Environment>) {
			entry.valueCarrier.close()
		}

	}

	fun getCarrier(key: Key) = recycler.get(key).valueCarrier

	operator override fun get (key: Key) = recycler.get(key).value

	fun recycle (isInUse: (Key) -> Boolean) = recycler.recycle(isInUse)

	fun recycleAll() = recycler.recycleAll()

	fun close() = recycler.close()


}